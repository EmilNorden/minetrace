cmake_minimum_required (VERSION 2.6)
project (minetrace)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_VERBOSE_MAKEFILE on)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules")

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  	# using Clang
 	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	# using GCC	
 	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  # using Intel C++
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  # using Visual Studio C++
endif()


add_executable (minetrace
	src/assert.cpp
	src/bbox.cpp
	src/binaryreader.cpp
	src/binaryutil.cpp
	src/bitmap_surface.cpp
	src/block_intersection_handler.cpp
	src/camera.cpp
	src/main.cpp
	src/material.cpp
	src/octree.cpp
	src/octreefactory.cpp
	src/output_helper.cpp
	src/raytracer.cpp
	src/region.cpp
	src/scene.cpp
	src/shaders.cpp
	src/texture.cpp
	src/texturemanager.cpp
	src/texturehandler.cpp
	src/world.cpp
	src/zlib.cpp
	src/nbt/nbt_tag.cpp
	src/nbt/nbt_payload.cpp
	src/nbt/nbt_parser.cpp
)

find_package(Boost 1.50 COMPONENTS filesystem iostreams system REQUIRED)
find_package(FreeImage REQUIRED)
find_package(ZLIB REQUIRED)
find_package(Threads REQUIRED)
include_directories(
	${Boost_INCLUDE_DIRS}
	${FreeImage_INCLUDE_DIRS}
	${zlib_INCLUDE_DIRS}
	src
)
target_link_libraries(minetrace
	${Boost_LIBRARIES}
	${FreeImage_LIBRARIES}
	${ZLIB_LIBRARIES}
	${CMAKE_THREAD_LIBS_INIT}
)
