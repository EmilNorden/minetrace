#include "scene.h"

std::shared_ptr<Camera> Scene::get_camera() const
{
	return camera_;
}

void Scene::set_camera(std::shared_ptr<Camera> &camera)
{
	camera_ = camera;
}