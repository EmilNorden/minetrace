#include "world.h"
#include "block.h"
#include "camera.h"
#include <cmath>
#include <map>
#include "region.h"
#include <memory>
#include <vector>
#include "binaryutil.h"
#include "octreefactory.h"

#include <iostream>

// This sorting might never be used.
class RegionSorter
{
private:
	Vector3d origin_;
public:
	RegionSorter(const Vector3d &origin)
		: origin_(origin)
	{}
	bool operator() (const Region &a, const Region &b)
	{
		return Vector3d::Distance(Vector3d(a.coordinate().X(), 0, a.coordinate().Y()), origin_) < Vector3d::Distance(Vector3d(b.coordinate().X(), 0, b.coordinate().Y()), origin_);
	}
};

void World::LoadRegions()
{
	const int ChunkRadius = 3; // 64;
	Vector3d camera_chunk = camera_->position();
	camera_chunk.X() = floor(camera_chunk.X() / 16.0f);
	camera_chunk.Z() = floor(camera_chunk.Z() / 16.0f);
	std::map<int64_t, Region> region;
	std::vector<int64_t> regions_to_load;

	for(int32_t x =  camera_chunk.X() - ChunkRadius; x < ChunkRadius + camera_chunk.X(); ++x)
	{
		for(int32_t z = camera_chunk.Z() - ChunkRadius; z < ChunkRadius + camera_chunk.Z(); ++z)
		{
			if(sqrt(pow(camera_chunk.X() - x, 2)  + pow(camera_chunk.Z() - z, 2)) <= ChunkRadius)
			{
				int32_t region_x = floor(x / 32.0f);
				int32_t region_z = floor(z / 32.0f);

				int64_t packed_coord = BinaryUtil::Pack(region_x, region_z);
				bool should_add = true;
				for(auto it = regions_to_load.begin(); it != regions_to_load.end(); ++it)
				{
					if(*it == packed_coord)
						should_add = false;
				}

				if(should_add)
					regions_to_load.push_back(packed_coord);
			}
		}
	}

	//regions_to_load.clear();
	//regions_to_load.push_back(BinaryUtil::Pack(0, 0));

	for(auto it = regions_to_load.begin(); it != regions_to_load.end(); ++it)
	{
		int32_t region_x, region_z;
		BinaryUtil::Unpack(*it, region_x, region_z);
		//if(region_x == 0 && region_z == 0)
		{
			std::shared_ptr<Region> region = std::shared_ptr<Region>(new Region);

			std::cout << "World: Loading region [" << region_x << ", " << region_z << "]" << std::endl;

			region->LoadFile(path_, region_x, region_z);
			if(region->loaded())
			{
				//loaded_regions_.push_back(region);

				std::shared_ptr<Octree<std::shared_ptr<Block>>> tree1;
				std::shared_ptr<Octree<std::shared_ptr<Block>>> tree2;
				std::shared_ptr<Octree<std::shared_ptr<Block>>> tree3;
				std::shared_ptr<Octree<std::shared_ptr<Block>>> tree4;
				OctreeFactory::CreateFromRegion(region, tree1, tree2, tree3, tree4);

				octrees_.push_back(tree1);
				octrees_.push_back(tree2);
				octrees_.push_back(tree3);
				octrees_.push_back(tree4);
			}
		}
	}
}

Vector2i World::region_to_chunk_coord(const Vector2i &coord)
{
	return coord * 32;
}

Vector2i World::chunk_to_block_coord(const Vector2i &coord)
{
	return coord * 16;
}

bool World::CastRay(Ray &ray, IntersectionInfo<std::shared_ptr<Block>> &intersection)
{
	bool has_intersection = false;
	for(auto octree_it = octrees_.begin(); octree_it != octrees_.end(); ++octree_it)
	{
		if((*octree_it)->CastRay(ray, intersection))
			has_intersection = true;
	}

	return has_intersection;
}

uint32_t World::block_count() const
{
	uint32_t total = 0;
	for(auto &octree : octrees_)
	{
		total += octree->count();
	}

	return total;
}
