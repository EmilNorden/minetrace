#ifndef BINARY_UTIL_H_
#define BINARY_UTIL_H_

#include <cstdint>

class BinaryUtil
{
public:
	template<typename T>
	static T SwapEndianness(T in)
	{
		return in; // by default, perform no byte order swap. The specialized template functions does this.
	}

	static int64_t Pack(int32_t a, int32_t b);
	static void Unpack(int64_t in, int32_t& a, int32_t& b);

	static int32_t Pack(int16_t a, int16_t b);
	static void Unpack(int32_t in, int16_t& a, int16_t& b);

	static int16_t Pack(int8_t a, int8_t b);
	static void Unpack(int16_t in, int8_t& a, int8_t& b);
};

template<>
inline
int16_t BinaryUtil::SwapEndianness<int16_t>(int16_t in)
{
	return ((in & 0x00FF) << 8) | ((in & 0xFF00) >> 8);
}

template<>
inline
int32_t BinaryUtil::SwapEndianness<int32_t>(int32_t in)
{
	return ((in & 0x0000FF00) << 8) | ((in & 0x00FF0000) >> 8) |
			((in & 0x000000FF) << 24) | ((in & 0xFF000000) >> 24);
}

template<>
inline
int64_t BinaryUtil::SwapEndianness<int64_t>(int64_t in)
{
	return ((in & 0x00000000000000FF) << 56) | ((in & 0xFF00000000000000) >> 56) | 
			((in & 0x000000000000FF00) << 40) | ((in & 0x00FF000000000000) >> 40) | 
			((in & 0x0000000000FF0000) << 24) | ((in & 0x0000FF0000000000) >> 24) |
			((in & 0x00000000FF000000) << 8) | ((in & 0x000000FF00000000) >> 8);
}

template<>
inline
float BinaryUtil::SwapEndianness<float>(float in)
{
	return static_cast<float>(BinaryUtil::SwapEndianness<int32_t>(static_cast<int32_t>(in)));
}

template<>
inline
double BinaryUtil::SwapEndianness<double>(double in)
{
	return static_cast<double>(BinaryUtil::SwapEndianness<int64_t>(static_cast<int64_t>(in)));
}

#endif
