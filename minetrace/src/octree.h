#ifndef OCTREE_H_
#define OCTREE_H_

#include <cstdint>
#include "intersection_info.h"
#include "octree_cuboid.h"
#include <vector>
#include "vector.h"
#include "ray.h"
#include "blocktypes.h"
#include "block_intersection_handler.h"

template <typename T>
class Octree
{
private:
	uint32_t count_;
	Cuboid<T> root_;

	void BuildInternal(Cuboid<T> &cuboid, size_t item_treshold, int &id);
	bool CastRay(Ray &ray, Cuboid<T> &cuboid, IntersectionInfo<T> &intersection);	
public:
	std::vector<T> items_;
	void Build(int item_treshold);
	void Build(int item_treshold, const BBox &bounds);
	bool CastRay(Ray &ray, IntersectionInfo<T> &intersection);
	uint32_t count() const { return count_; }
};

template <typename T>
void Octree<T>::Build(int item_treshold)
{
	BBox bounds;
	bounds.max_.X() = 0;
	bounds.max_.Y() = 0;
	bounds.max_.Z() = 0;

	bounds.min_.X() = 0;
	bounds.min_.Y() = 0;
	bounds.min_.Z() = 0;

	for(auto it = this->items_.begin(); it != this->items_.end(); ++it)
		bounds.Expand((*it).position());

	Build(item_treshold, bounds);
}

template <typename T>
void Octree<T>::Build(int item_treshold, const BBox &bounds)
{
	this->root_.bounds_ = bounds;
	this->root_.set_id(0);

	for(auto it = this->items_.begin(); it != this->items_.end(); ++it)
	{
		if(this->root_.bounds_.Contains((*it)->position()))
			this->root_.add(*it);
			//this->root_.items_.push_front(*it);
	}
	
	this->count_ = this->items_.size();
	this->items_.clear();
	this->items_.shrink_to_fit();

	int id = 0;
	this->BuildInternal(this->root_, item_treshold, id);
}

template <typename T>
void Octree<T>::BuildInternal(Cuboid<T> &cuboid, size_t item_treshold, int &id)
{
	//if(cuboid.items_.size() > item_treshold)
	if(cuboid.item_counter_ > item_treshold)
	{
		Vector3d size = (cuboid.bounds_.max_ - cuboid.bounds_.min_) / 2.0f;

		cuboid.children_.reset(new Cuboid<T>[8]);

		for(int x = 0; x < 2; ++x)
		{
			for(int y = 0; y < 2; ++y)
			{
				for(int z = 0; z < 2; ++z)
				{
					int index = (x * 4) + (y * 2) + z;
					cuboid.children_[index].bounds_.min_.X() = cuboid.bounds_.min_.X() + (size.X() * x);
					cuboid.children_[index].bounds_.min_.Y() = cuboid.bounds_.min_.Y() + (size.Y() * y);
					cuboid.children_[index].bounds_.min_.Z() = cuboid.bounds_.min_.Z() + (size.Z() * z);

					// Could use min calculation above, but i want to make the calculations independent.
					cuboid.children_[index].bounds_.max_.X() = cuboid.bounds_.min_.X() + (size.X() * (x+1));
					cuboid.children_[index].bounds_.max_.Y() = cuboid.bounds_.min_.Y() + (size.Y() * (y+1));
					cuboid.children_[index].bounds_.max_.Z() = cuboid.bounds_.min_.Z() + (size.Z() * (z+1));

					auto it = cuboid.items_.begin();
					auto prev_it = cuboid.items_.before_begin();
					while(it != cuboid.items_.end())
					{
						if(cuboid.children_[index].bounds_.Contains((*it)->position()))
						{
							cuboid.children_[index].add(*it);
							it = cuboid.items_.erase_after(prev_it);
							//cuboid.children_[index].items_.push_front(*it);
							//it = cuboid.items_.erase(it);
							
							cuboid.remove();
						}
						else
						{
							++it;
							++prev_it;
						}
					}

					cuboid.set_id(++id);
					if(cuboid.children_[index].item_counter_)//if(cuboid.items_.size())
					{
						this->BuildInternal(cuboid.children_[index], item_treshold, id);
					}
				}
			}
		}
	}
}

template <typename T>
bool Octree<T>::CastRay(Ray &ray, IntersectionInfo<T> &intersection)
{
	return CastRay(ray, root_, intersection);
}

template <typename T>
bool Octree<T>::CastRay(Ray &ray, Cuboid<T> &cuboid, IntersectionInfo<T> &intersection)
{
	bool has_intersection = false;
	BBoxFace cuboid_face;
	double cuboid_distance;
	if(cuboid.bounds_.Intersects(ray, cuboid_face, cuboid_distance) && ray.distance() > cuboid_distance)
	{
		if(cuboid.children_.get() == 0)
		{
			for(auto item_iterator = cuboid.items_.cbegin(); item_iterator != cuboid.items_.cend(); item_iterator++)
			{
				//BBox b((*item_iterator).position(), (*item_iterator).position() + Vector3f(1, 1, 1)); // Precalculate this?
				BBoxFace face;
				double distance;
				T item = *item_iterator;
				if(!item->ignore_collisions() && BlockIntersects(ray, item, face, distance) && ray.distance_ > distance)
				{
					ray.distance_ = distance;
					intersection.coordinate = ray.origin_ + (ray.direction_ * ray.distance_);
					
					float u, v;
					intersection.face = face;
					switch(face)
					{
					case FACE_PositiveY:
						intersection.normal = Vector3d(0, 1, 0);
						intersection.u = (intersection.coordinate.X() - item->position().X());
						intersection.u = MAX(intersection.u, 0);
						intersection.v = (intersection.coordinate.Z() - item->position().Z());
						break;
					case FACE_NegativeY:
						intersection.normal = Vector3d(0, -1, 0);
						intersection.u = (intersection.coordinate.X() - item->position().X());
						intersection.v = (intersection.coordinate.Z() - item->position().Z());
						break;
					case FACE_PositiveX:
						intersection.normal = Vector3d(1, 0, 0);
						intersection.u = intersection.coordinate.Z() - item->position().Z();
						intersection.v = intersection.coordinate.Y() - item->position().Y();
						break;
					case FACE_NegativeX:
						intersection.normal = Vector3d(-1, 0, 0);
						intersection.u = 1 - (intersection.coordinate.Z() - item->position().Z());
						intersection.v = intersection.coordinate.Y() - item->position().Y();
						break;
					case FACE_PositiveZ:
						intersection.normal = Vector3d(0, 0, 1);
						intersection.u = intersection.coordinate.X() - item->position().X();
						intersection.v = (intersection.coordinate.Y() - item->position().Y());
						break;
					case FACE_NegativeZ:
						intersection.normal = Vector3d(0, 0, -1);
						intersection.u = 1 - (intersection.coordinate.X() - item->position().X());
						intersection.v = (intersection.coordinate.Y() - item->position().Y());
						break;
					}
					intersection.u = MAX(intersection.u, 0);
					intersection.v = MAX(intersection.v, 0);
					intersection.u = MIN(intersection.u, 1);
					intersection.v = MIN(intersection.v, 1);
					
					has_intersection = true;
					
					intersection.object = item;
				}
			}
		}
		else
		{
			for(int i = 0; i < 8; ++i)
			{
				if(CastRay(ray, cuboid.children_[i], intersection))
					has_intersection = true;
			}
		}
	}

	return has_intersection;
}


#endif
