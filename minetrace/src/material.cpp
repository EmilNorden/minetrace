#include "material.h"
#include "texture.h"

const Texture *Material::diffuse() const
{
	return diffuse_;
}
const Texture *Material::emissive() const
{
	return emissive_;
}