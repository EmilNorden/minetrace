#include "../massert.h"
#include "nbt_payload.h"
#include "nbt_tag.h"
#include "../exceptions.h"
#include <boost/function.hpp>
#include <boost/variant.hpp>

//class NBTPayload::impl
//{
//public:
//	/*boost::variant<int8_t, int16_t, int32_t, int64_t, float, double, boost::shared_array<int8_t>,
//		std::string, std::vector<NBTPayload>, boost::shared_array<int32_t>> union_;*/
//};

NBTPayload::NBTPayload()
	: type_(NBT_None)//, //pimpl_(new impl)
{
}

NBTPayload::~NBTPayload()
{
}


void NBTPayload::set_data(int8_t i8) 
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Byte;
	
	union_ = i8;
	ASSERT(union_.which() == 0);
}

void NBTPayload::set_data(int16_t i16)
	{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Short;
	
	union_ = i16;
	ASSERT((union_.which() == 1));
}

void NBTPayload::set_data(int32_t i32)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Int;
	
	union_ = i32;
	ASSERT((union_.which() == 2));
}

void NBTPayload::set_data(int64_t i64)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Long;
	
	union_ = i64;
	ASSERT((union_.which() == 3));
}

void NBTPayload::set_data(float f32)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Float;
	
	union_ = f32;
	ASSERT(union_.which() == 4);
}

void NBTPayload::set_data(double f64)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Double;
	
	union_ = f64;
	ASSERT(union_.which() == 5);
}

void NBTPayload::set_data(boost::shared_array<int8_t> &i8_arr)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_ByteArray;
	
	union_ = i8_arr;
	ASSERT(union_.which() == 6);
}

void NBTPayload::set_data(const std::string &str)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_String;
	
	union_ = str;
	ASSERT(union_.which() == 7);
}

void NBTPayload::set_data(const std::vector<NBTPayload> list)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_List;
	
	union_ = list;
	ASSERT(union_.which() == 8);
}

void NBTPayload::set_data(const std::vector<NBTTagPtr> list)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_Compound;
	
	union_ = list;
	ASSERT(union_.which() == 9);
}

void NBTPayload::set_data(boost::shared_array<int32_t> &i32_arr)
{
	if(type_ != NBT_None)
		throw InvalidOperationException("NBTPayload already has a value.");
	
	type_ = NBT_IntArray;
	
	union_ = i32_arr;
	ASSERT(union_.which() == 10);
}
