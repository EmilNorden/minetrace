#include "nbt_parser.h"

NBTTagPtr NBTParser::Parse(std::istream &s)
{
	BinaryReader<true> reader(s);
	
	return Parse(reader);
}

NBTTagPtr NBTParser::Parse(BinaryReader<true> &reader, NBTTagPtr parent)
{
	NBTTagPtr tag(new NBTTag(parent));
	tag.get()->set_type(static_cast<NBTTagType>(reader.ReadByte()));

	if(tag->type() != NBT_End)
	{
		tag->set_name(reader.ReadString());

		ParsePayload(reader, tag->payload_, tag->type(), tag);
	}

	return tag;
}

void NBTParser::ParsePayload(BinaryReader<true> &reader, NBTPayload &payload, NBTTagType type, NBTTagPtr tag)
{
	switch(type)
	{
	case NBT_Byte:
		payload.set_data(reader.ReadByte());
		break;
	case NBT_Short:
		payload.set_data(reader.ReadInt16());
		break;
	case NBT_Int:
		payload.set_data(reader.ReadInt32());
		break;
	case NBT_Long:
		payload.set_data(reader.ReadInt64());
		break;
	case NBT_Float:
		payload.set_data(reader.ReadFloat());
		break;
	case NBT_Double:
		payload.set_data(reader.ReadDouble());
		break;
	case NBT_ByteArray:
		{
			int32_t array_size = reader.ReadInt32();
			boost::shared_array<int8_t> array = reader.ReadArray<int8_t>(array_size);
			payload.set_data(array);
		}
		break;
	case NBT_String:
		payload.set_data(reader.ReadString());
		break;
	case NBT_List:
		{
			std::vector<NBTPayload> payloads;
			NBTTagType list_type  = (NBTTagType)reader.ReadByte();
			int32_t list_size = reader.ReadInt32();

			for(int i = 0; i < list_size; ++i)
			{
				NBTPayload p;
				ParsePayload(reader, p, list_type);
				payloads.push_back(p);
			}
			payload.set_subtype(list_type);
			payload.set_data(payloads);
		}
		break;
	case NBT_Compound:
		{
			std::vector<NBTTagPtr> tags;
			NBTTagPtr child;
			while((child = Parse(reader, tag))->type() != NBT_End)
				tags.push_back(child);

			payload.set_data(tags);
		}
		break;
	case NBT_IntArray:
		{
			int32_t array_size = reader.ReadInt32();
			boost::shared_array<int32_t> array = reader.ReadArray<int32_t>(array_size);
			payload.set_data(array);
		}
		break;
	}
}
