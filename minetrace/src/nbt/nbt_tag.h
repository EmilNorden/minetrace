#ifndef NBTTAG_H_
#define NBTTAG_H_

#include <string>
#include <cstdint>
#include <memory>
#include "nbt_tagtype.h"
#include "nbt_payload.h"

class NBTTag
{
	friend class NBTParser;
private:
	std::string name_;
	NBTTagType type_;
	std::shared_ptr<NBTTag> parent_;
	
public:
	NBTPayload payload_;
	const std::string name() const { return name_; }
	void set_name(const std::string& name) { name_ = name; }
	NBTTagType type() const { return type_; }
	void set_type(NBTTagType type) { type_ = type; }

	std::shared_ptr<NBTTag> Child(const std::string &name);

	NBTTag &Node(const std::string &name) const;

	NBTTag(std::shared_ptr<NBTTag> parent = nullptr);
	~NBTTag();
};

typedef std::shared_ptr<NBTTag> NBTTagPtr;

#endif