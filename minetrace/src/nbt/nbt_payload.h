#ifndef NBT_PAYLOAD_H_
#define NBT_PAYLOAD_H_

#include <boost/variant.hpp>
#include <boost/shared_array.hpp>
#include <string>
#include <cstdint>
#include "nbt_tagtype.h"
#include <vector>
#include <memory>

class NBTTag;

class NBTPayload 
{
	friend class NBTParser;

private:
	NBTTagType type_;
	NBTTagType subtype_; // only applicable if type_ == NBT_List


	boost::variant<int8_t, int16_t, int32_t, int64_t, float, double, boost::shared_array<int8_t>,
		std::string, std::vector<NBTPayload>, std::vector<std::shared_ptr<NBTTag>>, boost::shared_array<int32_t>> union_;

	void set_data(int8_t i8);
	void set_data(int16_t i16);
	void set_data(int32_t i32);
	void set_data(int64_t i64);
	void set_data(float f32);
	void set_data(double f64);
	void set_data(boost::shared_array<int8_t> &i8_arr);
	void set_data(const std::string &str);
	void set_data(const std::vector<NBTPayload> list);
	void set_data(const std::vector<std::shared_ptr<NBTTag>> list);
	void set_data(boost::shared_array<int32_t> &i32_arr);

	
public:
	NBTPayload();
	~NBTPayload();

	template <typename T>
	T get_data();

	NBTTagType type() const { return type_; }
	NBTTagType subtype() const { return subtype_; }
	void set_subtype(NBTTagType type) { subtype_ = type; }
};

template <typename T>
T NBTPayload::get_data()
{
	return boost::get<T>(union_);
}

#endif