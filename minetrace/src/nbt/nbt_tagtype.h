#ifndef NBT_TAGTYPE_H_
#define NBT_TAGTYPE_H_

#include <cstdint>

#ifdef _MSC_VER

#pragma warning(push)
#pragma warning( disable : 4480 ) // disable warning that is generated when explicitly specifying underlying enum type. This is a C++11 feature and shouldn't generate a warning.
#pragma warning( disable : 4341 ) // disable warning: 'NBT_None' : signed value is out of range for enum constant. Possibly MSVC bug?

#endif

enum NBTTagType : int8_t
{
	NBT_None = -1,
	NBT_End,
	NBT_Byte,
	NBT_Short,
	NBT_Int,
	NBT_Long,
	NBT_Float,
	NBT_Double,
	NBT_ByteArray,
	NBT_String,
	NBT_List,
	NBT_Compound,
	NBT_IntArray,
	NBT_TypeCount
};

#ifdef _MSC_VER

#pragma warning(pop)

#endif

#endif