#ifndef NBT_PARSER_H_
#define NBT_PARSER_H_

#include "nbt_tag.h"
#include "nbt_payload.h"
#include <istream>

#include "../binaryreader.h"

class NBTParser
{
private:
	void ParsePayload(BinaryReader<true> &reader, NBTTagPtr &tag);
	void ParsePayload(BinaryReader<true> &reader, NBTPayload &payload, NBTTagType type, NBTTagPtr tag = nullptr);
public:
	NBTTagPtr Parse(std::istream &s);
	NBTTagPtr Parse(BinaryReader<true> &reader, NBTTagPtr parent = nullptr);
};

#endif