#include "nbt_tag.h"
#include <iostream>

NBTTag::NBTTag(std::shared_ptr<NBTTag> parent)
	: parent_(parent)
{
}

NBTTag::~NBTTag()
{
	//std::cout << "Tag " << name_ << " destroyed" << std::endl;
}



NBTTagPtr NBTTag::Child(const std::string &name)
{
	if(type_ == NBT_Compound)
	{
		std::vector<std::shared_ptr<NBTTag>> children = payload_.get_data<std::vector<std::shared_ptr<NBTTag>>>();
		for(auto it = children.begin(); it != children.end(); ++it)
		{
			if((*it)->name() == name)
				return *it;
		}
	}
	//else if(type_ == NBT_List && payload_.subtype() == NBT_Compound)
	//{
	//	std::vector<NBTPayload> list = payload_.get_data<std::vector<NBTPayload>>();
	//	for(auto it = list.begin(); it != list.end(); ++it)
	//	{
	//		auto ff = *it;
	//		auto asd = ff.get_data<std::vector<std::shared_ptr<NBTTag>>>();
	//		int ffs = 5;
	//	}
	//}
	return nullptr;
}