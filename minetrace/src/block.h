#ifndef BLOCK_H_
#define BLOCK_H_

#include "blocktypes.h"
#include "vector.h"
#include <cstdint>

class Block
{
private:
	BlockType type_;
	int8_t data_;
	Vector3d position_;
	bool ignore_collisions_;
public:
	bool ignore_collisions() const { return ignore_collisions_; }
	void set_ignore_collisions(bool ignore) { ignore_collisions_ = ignore; }
	BlockType type() const { return type_; }
	int8_t data() const { return data_; }
	Vector3d position() const { return position_; }

	Block(BlockType type, int8_t data, const Vector3d &position)
		: type_(type), data_(data), position_(position), ignore_collisions_(false)
	{}
};

#endif