#ifndef TEXTURE_MANAGER_H_
#define TEXTURE_MANAGER_H_

// Forward declarations
class FIBITMAP;
class Texture;

#include <map>
#include <string>
#include "vector.h"

// forward declarations
namespace boost
{
	namespace filesystem
	{
		class path;
	}
}


class TextureManager
{
public:
	enum TextureType
	{
		TEX_BLOCK,
		TEX_ITEM
	};

	static void Initialize();
	static const Texture *GetTexture(TextureType type, const std::string &name);
	static Texture *CreateTinted(const Texture* texture, const Vector4d &tint);
	static void RegisterTexture(Texture *texture, TextureType type, const std::string &name);
	static void Finalize();
private:
	static std::map<std::string, Texture*> block_textures_;
	static std::map<std::string, Texture*> item_textures_;

	static const Texture *GetTextureInternal(const std::map<std::string, Texture*> &texture_map, const std::string &name);
	static void LoadTextures(std::map<std::string, Texture*> &texture_map, boost::filesystem::path& folder);
};

#endif
