#ifndef VECTOR_H_
#define VECTOR_H_

#include <cmath>
#include <cstddef>
#include <cstdint>

template <size_t N, typename T>
class Vector
{
public:

	Vector()
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] = 0;
	}

	Vector(const T v[N])
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] = v[i];
	}

	// Ugly convenience constructors, Can this be done more elegant?
	Vector(T x, T y)
	{
		static_assert(N > 1, "Dimensionality of vector too low to invoke this constructor");
		v_[0] = x;
		v_[1] = y;
	}

	Vector(T x, T y, T z)
	{
		static_assert(N > 2, "Dimensionality of vector too low to invoke this constructor");
		v_[0] = x;
		v_[1] = y;
		v_[2] = z;
	}

	Vector(T x, T y, T z, T w)
	{
		static_assert(N > 3, "Dimensionality of vector too low to invoke this constructor");
		v_[0] = x;
		v_[1] = y;
		v_[2] = z;
		v_[3] = w;
	}


	// copy constructor
	Vector<N, T>(const Vector<N, T> &other)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] = other.v_[i];
	}


	T v_[N];

	//// convenience methods
		inline
		T& X() {
			static_assert(N > 0, "Dimensionality of vector too low to access X() member");
			return v_[0];
	}

		inline
		T X() const {
			static_assert(N > 0, "Dimensionality of vector too low to access X() member");
			return v_[0];
	}

		inline
		T& Y() {
			static_assert(N > 1, "Dimensionality of vector too low to access Y() member");
			return v_[1];
	}

		inline
		T Y() const {
			static_assert(N > 1, "Dimensionality of vector too low to access Y() member");
			return v_[1];
	}

		inline
		T& Z() {
			static_assert(N > 2, "Dimensionality of vector too low to access Z() member");
			return v_[2];
	}

		inline
		T Z() const {
			static_assert(N > 2, "Dimensionality of vector too low to access Z() member");
			return v_[2];
	}

		inline
		T& W() {
			static_assert(N > 3, "Dimensionality of vector too low to access W() member");
			return v_[3];
	}

		inline
		T W() const {
			static_assert(N > 3, "Dimensionality of vector too low to access W() member");
			return v_[3];
	}

	// operator overloads

	inline
		bool operator==(const Vector<N, T> &other) const
	{
		for(size_t i = 0; i < N; ++i)
		{
			if(this->v_[i] != other.v_[i])
				return false;
		}
		return true;
	}

	
	
	inline
		bool operator!=(const Vector<N, T> &other) const
	{
		for(size_t i = 0; i < N; ++i)
		{
			if(this->v_[i] != other.v_[i])
				return true;
		}
		return false;
	}

	inline
		Vector<N, T> operator+(const Vector<N, T> &other) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] + other.v_[i];
		return result;
	}

	inline
		Vector<N, T> operator-(const Vector<N, T> &other) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] - other.v_[i];
		return result;
	}

	inline
		Vector<N, T> operator/(const Vector<N, T> &other) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] / other.v_[i];
		return result;
	}

	inline
		Vector<N, T> operator/(T divisor) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] / divisor;
		return result;
	}

	inline
		Vector<N, T> operator*(const Vector<N, T> &other) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] * other.v_[i];
		return result;
	}

	inline
		Vector<N, T> operator*(T factor) const
	{
		Vector result;
		for(size_t i = 0; i < N; ++i)
			result.v_[i] = this->v_[i] * factor;
		return result;
	}

	inline
		Vector<N, T> &operator+=(const Vector<N, T> &other)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] += other.v_[i];

		return *this;
	}

	inline
		Vector<N, T> &operator-=(const Vector<N, T> &other)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] -= other.v_[i];

		return *this;
	}

	inline
		Vector<N, T> &operator/=(const Vector<N, T> &other)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] /= other.v_[i];

		return *this;
	}

	inline
		Vector<N, T> &operator*=(const Vector<N, T> &other)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] *= other.v_[i];

		return *this;
	}

	inline
		Vector<N, T> &operator*=(T factor)
	{
		for(size_t i = 0; i < N; ++i)
			this->v_[i] *= factor;

		return *this;
	}

	Vector<N, T> Cross(const Vector<N, T> &other) const {
		static_assert(N == 3, "Cross operation is only implemented for 3D vectors");

		return Vector<N, T>(Y() * other.Z() - Z() * other.Y(),
			Z() * other.X() - X() * other.Z(),
			X() * other.Y() - Y() * other.X());
	}

	T Length() const
	{
		static_assert(N > 2, "Dimensionality of vector too low to invoke this constructor");

		return sqrt(pow(v_[0], 2) + pow(v_[1], 2) + pow(v_[2], 2));
	}

	inline
		void Normalize()
	{
		T length = 0;
		for(size_t i = 0; i < N; ++i)
			length += this->v_[i]*this->v_[i];
		length = sqrt(length);
		length = 1.0 / length;

		for(size_t i= 0; i < N; ++i)
			this->v_[i] *= length;
	}
	static void Add(const Vector<N, T> &a, const Vector<N, T> &b, Vector<N, T> &result)
	{
		for(size_t i = 0; i < N; ++i)
		{
			result.v_[i] = a.v_[i] + b.v_[i];
		}
	}

	static void Divide(const Vector<N, T> &vector, T divisor, Vector<N, T> &result)
	{
		for(size_t i = 0; i < N; ++i)
		{
			result.v_[i] = vector.v_[i] / divisor;
		}
	}

	// Methods specific for 3-dimensional vectors
	static Vector<3, T> Cross(const Vector<3, T> &a, const Vector<3, T> &b)
	{
		Vector<3, T> result;
		result.X() = ((double) a.Y() * (double) b.Z() - (double) a.Z() * (double) b.Y());
		result.Y() = ((double) a.Z() * (double) b.X() - (double) a.X() * (double) b.Z());
		result.Z() = ((double) a.X() * (double) b.Y() - (double) a.Y() * (double) b.X());
		return result;
	}

		static void Multiply(const Vector<N, T> *a, T factor, Vector<N, T> *result)
	{
		for(size_t i = 0; i < N; ++i)
		{
			result->v_[i] = a->v_[i] * factor;
		}
	}

	static void Subtract(const Vector<N, T> &a, const Vector<N, T> &b, Vector<N, T> &result)
	{
		for(size_t i = 0; i < N; ++i)
		{
			result.v_[i] = a.v_[i] - b.v_[i];
		}
	}

	static T Dot(const Vector<N, T> &a, const Vector<N, T> &b)
	{
		T dot = 0;
		for(size_t i = 0; i < N; ++i)
			dot += a.v_[i] * b.v_[i];

		return dot;
	}

	static T Distance(const Vector<N, T> &a, const Vector<N, T> &b)
	{
		T sum = 0.0f;
		for(size_t i = 0; i < N; ++i)
		{
			sum += pow(a.v_[i] - b.v_[i], 2);
		}

		return sqrt(sum);
	}

	static void Lerp(const Vector<N, T> &a, const Vector<N, T> &b, Vector<N, T> &result, float factor)
	{
		for(size_t i = 0; i < N; ++i)
		{
			result.v_[i] = (factor * b.v_[i]) + ((1 - factor) * a.v_[i]);
		}
	}
};

typedef Vector<2, float> Vector2f;
typedef Vector<3, float> Vector3f;
typedef Vector<4, float> Vector4f;
typedef Vector<2, double> Vector2d;
typedef Vector<3, double> Vector3d;
typedef Vector<4, double> Vector4d;
typedef Vector<2, int32_t> Vector2i;
typedef Vector<2, uint32_t> Vector2ui;
typedef Vector<3, int32_t> Vector3i;
typedef Vector<4, int32_t> Vector4i;

//template <size_t N, typename T>
//void Vector<N, T>::Transform(const Matrix& matrix, Vector<3, T> &result) const
//	{
//		static_assert(N > 2, "Dimensionality of vector too low to invoke Transform()");
//		float num1 = (float) ((double) v_[0] * (double) matrix.v_[0][0] + (double) v_[1] * (double) matrix.v_[1][0] + (double) v_[2] * (double) matrix.v_[2][0]) + matrix.v_[3][0];
//		float num2 = (float) ((double) v_[0] * (double) matrix.v_[0][1] + (double) v_[1] * (double) matrix.v_[1][1] + (double) v_[2] * (double) matrix.v_[2][1]) + matrix.v_[3][1];
//		float num3 = (float) ((double) v_[0] * (double) matrix.v_[0][2] + (double) v_[1] * (double) matrix.v_[1][2] + (double) v_[2] * (double) matrix.v_[2][2]) + matrix.v_[3][2];
//		result.X() = num1;
//		result.Y() = num2;
//		result.Z() = num3;
//	}


#endif
