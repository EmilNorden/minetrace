#include "bitmap_surface.h"
#include "massert.h"
#include "freeimage/FreeImage.h"

void BitmapSurface::InitBitmap()
{
	if(bmp_)
	{
		FreeImage_Unload(bmp_);
		bmp_ = nullptr;
	}

	bmp_ = FreeImage_Allocate(width_, height_, 24);
}

BitmapSurface::BitmapSurface(uint32_t width, uint32_t height)
	: width_(width), height_(height), bmp_(nullptr)
{
	InitBitmap();
}

void BitmapSurface::SetPixel(uint32_t x, uint32_t y, const Vector4d &color)
{
	ASSERT(x >= 0 && x < width_);
	ASSERT(y >= 0 && y < height_);

	RGBQUAD rgb;
	rgb.rgbRed = color.X() * 255;
	rgb.rgbGreen = color.Y() * 255;
	rgb.rgbBlue = color.Z() * 255;
	rgb.rgbReserved = color.W() * 255;
	FreeImage_SetPixelColor(bmp_, x, y, &rgb);
}

void BitmapSurface::GetPixel(uint32_t x, uint32_t y, Vector4d &color)
{
	RGBQUAD rgb;
	FreeImage_GetPixelColor(bmp_, x, y, &rgb);
	color.X() = rgb.rgbRed / 255.0f;
	color.Y() = rgb.rgbGreen / 255.0f;
	color.Z() = rgb.rgbBlue / 255.0f;
	color.W() = rgb.rgbReserved / 255.0f;
}

void BitmapSurface::save(const std::string &path)
{
	BOOL result = FreeImage_Save(FIF_PNG, bmp_, path.c_str());
	ASSERT(result);
}
