#include "block_intersection_handler.h"
#include "block_face.h"
#include "texture.h"
#include "assert.h"
#include "bbox.h"
#include "blocktypes.h"
#include "texturehandler.h"
#include <string>
bool is_yface(BBoxFace &face)
{
	return face == FACE_PositiveY || face == FACE_NegativeY;
}

LODTexturePtr get_block_texture(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<TextureHandler> &textures)
{
	BlockType type = intersection.object->type();
	int8_t block_data = intersection.object->data();
	BBoxFace face = intersection.face;
	std::string texture_name;

	switch(type)
	{
	case BT_Water:
	case BT_StationaryWater:
		texture_name = "water1";
		break;
	case BT_Lava:
		texture_name = "lava1";
		break;
	case BT_Bookshelf:
		if(is_yface(face))
			texture_name = "oak_planks";
		else
			texture_name = "book_case";
		break;
	
	case BT_Cactus:
		if(face == FACE_PositiveY)
			texture_name = "cactus_top";
		else if(face == FACE_NegativeY)
			texture_name = "cactus_bottom";
		else
			texture_name = "cactus_side";
		break;
	case BT_GrassBlock:
		if(face == FACE_PositiveY)
			texture_name = "grass_top_tintable";
		else if(face == FACE_NegativeY)
			texture_name = "dirt";
		else
			texture_name = "grass_side";
		break;
	case BT_WoodPlanks:
		texture_name = "oak_planks";
		break;
	case BT_Cobblestone:
		texture_name = "cobblestone";
		break;
	case BT_Sand:
		texture_name = "sand";
	}

	LODTexturePtr tex = textures->GetTexture(texture_name);

	return tex;
}

const Texture *HandleBlockIntersection(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection)
{
	/*BlockType type = intersection.object->type();
	int8_t block_data = intersection.object->data();
	BBoxFace face = intersection.face;
	const Texture *tex = nullptr;
	switch(type)
	{
	case BT_Stone:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stone");
		break;
	case BT_GrassBlock:
		switch(intersection.face)
		{
		case FACE_PositiveY:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "green_grass_top");
			break;
		case FACE_NegativeY:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dirt");
			break;
		default:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "grass_side");
		}
		break;
	case BT_Dirt:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dirt");
		break;
	case BT_Cobblestone:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebrick");
		break;
	case BT_WoodPlanks:
		switch(block_data)
		{
		case BD_Wood_Oak:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood");
			break;
		case BD_Wood_Spruce:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood_spruce");
			break;
		case BD_Wood_Birch:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood_birch");
			break;
		case BD_Wood_Jungle:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood_jungle");
			break;
		}
		break;
	case BT_Saplings:
		switch(block_data)
		{
		case BD_Wood_Oak:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sapling");
			break;
		case BD_Wood_Spruce:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sapling_spruce");
			break;
		case BD_Wood_Birch:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sapling_birch");
			break;
		case BD_Wood_Jungle:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sapling_jungle");
			break;
		}
		break;
	case BT_Bedrock:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "bedrock");
		break;
	case BT_Water:
		// TODO: fix water properly
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "water");
		break;
	case BT_StationaryWater:
		// TODO: fix water properly
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "water");
		break;
	case BT_Lava:
		// TODO: fix lava properly
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "lava");
		break;
	case BT_StationaryLava:
		// TODO: fix lava properly
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "lava");
		break;
	case BT_Sand:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sand");
		break;
	case BT_Gravel:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "gravel");
		break;
	case BT_GoldOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreGold");
		break;
	case BT_IronOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreIron");
		break;
	case BT_CoalOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreCoal");
		break;
	case BT_Wood:
		{
			int8_t wood_dir = block_data >> 2;
			int8_t wood_type = block_data & 0x3;
			if((wood_dir == BD_Wood_Dir_EastWest && (face == FACE_NegativeX || face == FACE_PositiveX)) ||
				(wood_dir == BD_Wood_Dir_NorthSouth && (face == FACE_NegativeZ || face == FACE_PositiveZ)) ||
				(wood_dir == BD_Wood_Dir_Normal && (face == FACE_NegativeY || face == FACE_PositiveY)))
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_top");
			}
			else
			{
				switch(wood_type)
				{
					case BD_Wood_Oak:
						tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_side");
						break;
					case BD_Wood_Spruce:
						tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_spruce");
						break;
					case BD_Wood_Birch:
						tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_birch");
						break;
					case BD_Wood_Jungle:
						tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_jungle");
						break;
				}
			}		
		}
		break;
	case BT_Leaves:
		{
			int8_t leaves_type = block_data & 0x3;
			switch(leaves_type)
			{
				case BD_Wood_Oak:
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "leaves");
					break;
				case BD_Wood_Spruce:
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "leaves_spruce");
					break;
				case BD_Wood_Birch:
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "leaves");
					break;
				case BD_Wood_Jungle:
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "leaves_jungle");
					break;
			}
		}
		break;
	case BT_Sponge:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sponge");
		break;
	case BT_Glass:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "glass");
		break;
	case BT_LapisLazuliOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreLapis");
		break;
	case BT_LapisLazuliBlock:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockLapis");
		break;
	case BT_Dispenser:
		if(block_data == BD_Block_Dir_Up)
		{
			if(face == FACE_PositiveY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dispenser_front_vertical");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_top");
		}
		else if(block_data == BD_Block_Dir_Down)
		{
			if(face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dispenser_front_vertical");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_top");
		}
		else
		{
			if(face == FACE_PositiveY || face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_top");
			else if(block_data == BD_Block_Dir_East && face == FACE_PositiveX ||
				block_data == BD_Block_Dir_West && face == FACE_NegativeX ||
				block_data == BD_Block_Dir_South && face == FACE_PositiveZ ||
				block_data == BD_Block_Dir_North && face == FACE_NegativeZ)
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dispenser_front");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_slide");
			}
		}
		break;
	case BT_Sandstone:
		switch(block_data)
		{
		case BD_Sandstone_Normal:
			if(face == FACE_PositiveY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_top");
			else if(face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_bottom");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_side");
			break;
		case BD_Sandstone_Chiseled:
			if(face == FACE_PositiveY || face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_top");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_carved");
			break;
		case BD_Sandstone_Smooth:
			if(face == FACE_PositiveY || face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_top");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_smooth");
			break;
		}
		break;
	case BT_NoteBlock:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "musicBlock");
		break;
	case BT_Bed:
		{
			// TODO: Fix proper bed texturing. Need to be able to specify orientation of texture?
			if(block_data & BD_Bed_Head_Flag)
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockLapis");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockGold");
			}
		}
		break;
	case BT_PoweredRail:
		// TODO: Fix powered rail
		break;
	case BT_DetectorRail:
		// TODO: Fix detector rail
		break;
	case BT_StickyPiston:
		// TODO: Fix sticky piston
		break;
	case BT_Cobweb:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "web");
		break;
	case BT_Grass:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tallgrass");
		break;
	case BT_DeadBush:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "deadbush");
		break;
	case BT_Piston:
		break;
	case BT_PistonExtension:
		break;
	case BT_Wool:
		switch(block_data)
		{
		case BD_Wool_White:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_0");
			break;
		case BD_Wool_Orange:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_1");
			break;
		case BD_Wool_Magenta:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_2");
			break;
		case BD_Wool_LightBlue:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_3");
			break;
		case BD_Wool_Yellow:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_4");
			break;
		case BD_Wool_Lime:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_5");
			break;
		case BD_Wool_Pink:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_6");
			break;
		case BD_Wool_Gray:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_7");
			break;
		case BD_Wool_LightGray:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_8");
			break;
		case BD_Wool_Cyan:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_9");
			break;
		case BD_Wool_Purple:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_10");
			break;
		case BD_Wool_Blue:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_11");
			break;
		case BD_Wool_Brown:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_12");
			break;
		case BD_Wool_Green:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_13");
			break;
		case BD_Wool_Red:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_14");
			break;
		case BD_Wool_Black:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cloth_15");
			break;
		}
		break;
	case BT_BlockMovedByPiston:
		break;
	case BT_Dandelion:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "flower");
		break;
	case BT_Rose:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "rose");
		break;
	case BT_BrownMushroom:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "mushroom_brown");
		break;
	case BT_RedMushroom:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "mushroom_red");
		break;
	case BT_BlockOfGold:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockGold");
		break;
	case BT_BlockOfIron:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockIron");
		break;
	case BT_DoubleSlabs:
	case BT_Slabs:
		switch(block_data)
		{
		case BD_Slab_Stone:
			if(face == FACE_PositiveY || face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stoneslab_top");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stoneslab_side");
			break;
		case BD_Slab_Sandstone:
			if(face == FACE_PositiveY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "sandstone_top");
			else if(face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stoneslab_bottom");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stoneslab_side");
			break;
		case BD_Slab_Cobblestone:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebrick");
			break;
		case BD_Slab_Brick:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "brick");
		case BD_Slab_StoneBrick:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebricksmooth");
		case BD_Slab_NetherBrick:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "netherBrick");
		case BD_Slab_Quartz:
			if(face == FACE_PositiveY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "quartzblock_top");
			else if(face == FACE_NegativeY)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "quartzblock_bottom");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "quartzblock_side");
			break;
		case BD_Slab_SmoothStone:
			 // TODO: Unused?
			break;
		case BD_Slab_SmoothSandstone:
			 // TODO: What block is this?
			break;
		case BD_Slab_TileQuartz:
			 // TODO: And this?
			break;
		}
		break;
	case BT_Bricks:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "brick");
		break;
	case BT_TNT:
		if(face == FACE_PositiveY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tnt_top");
		else if(face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tnt_bottom");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tnt_side");
		break;
	case BT_Bookshelf:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "bookshelf");
		break;
	case BT_MossStone:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stoneMoss");
		break;
	case BT_Obsidian:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "obsidian");
		break;
	case BT_Torch:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "torch");
		break;
	case BT_Fire:
		// TODO: fix fire. There is a fire_1 texture also.
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "fire_0");
		break;
	case BT_MonsterSpawner:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "mobSpawner");
		break;
	case BT_OakWoodStairs:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood");
		break;
	case BT_Chest:
		tex = TextureManager::GetTexture(TextureManager::TEX_ITEM, "chest");
		break;
	case BT_RedstoneWire:
		// TODO: Wire texture is either 'redstoneDust_line' or 'redstoneDust_cross', and orientation is set at runtime. Fix.
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "redstoneDust_line");
		break;
	case BT_DiamondOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreDiamond");
		break;
	case BT_BlockOfDiamond:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockDiamond");
		break;
	case BT_Craftingtable:
		if(face == FACE_NegativeZ || face == FACE_NegativeX)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "workbench_front");
		else if(face == FACE_PositiveZ || face == FACE_PositiveX)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "workbench_side");
		else if(face == FACE_PositiveY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "workbench_top");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood");	
		break;
	case BT_Wheat:
		switch(block_data)
		{
		case 0:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_0");
			break;
		case 1:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_1");
			break;
		case 2:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_2");
			break;
		case 3:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_3");
			break;
		case 4:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_4");
			break;
		case 5:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_5");
			break;
		case 6:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_6");
			break;
		case 7:
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "crops_7");
			break;
		}
		break;
	case BT_Farmland:
		if(face == FACE_PositiveY)
		{
			if(block_data == 0)
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "farmland_dry");
			else
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "farmland_wet");
		}
		else
		{
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dirt");
		}
		break;
	case BT_Furnace:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_top");
		else
		{
			if((block_data == BD_Block_Dir_East && face == FACE_PositiveX) ||
				(block_data == BD_Block_Dir_West && face == FACE_NegativeX) ||
				(block_data == BD_Block_Dir_South && face == FACE_PositiveZ) ||
				(block_data == BD_Block_Dir_North && face == FACE_NegativeZ))
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_front");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_side");
			}
		}
		break;
	case BT_BurningFurnace:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_top");
		else
		{
			if((block_data == BD_Block_Dir_East && face == FACE_PositiveX) ||
				(block_data == BD_Block_Dir_West && face == FACE_NegativeX) ||
				(block_data == BD_Block_Dir_South && face == FACE_PositiveZ) ||
				(block_data == BD_Block_Dir_North && face == FACE_NegativeZ))
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_front_lit");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "furnace_side");
			}
		}
		break;
	case BT_SignPost:
		tex = TextureManager::GetTexture(TextureManager::TEX_ITEM, "sign");
		break;
	case BT_WoodenDoor:
		// TODO: Handle doors once i have figured out how to handle non-blocky blocks.
		break;
	case BT_Ladders:
		// TODO: Handle ladders once i have figured out how to handle non-blocky blocks.
		break;
	case BT_Rail:
		// TODO: Handle rails once i have figured out how to handle non-blocky blocks.
		break;
	case BT_CobblestoneStairs:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebrick");
		break;
	case BT_WallSign:
		tex = TextureManager::GetTexture(TextureManager::TEX_ITEM, "sign");
		break;
	case BT_Lever:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "lever");
		break;
	case BT_StonePressurePlate:
		break;
	case BT_IronDoor:
		break;
	case BT_WoodenPressurePlate:
		break;
	case BT_RedstoneOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreRedstone");
		break;
	case BT_GlowingRedstoneOre:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "oreRedstone");
		break;
	case BT_RedstoneTorchInactive:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "redtorch");
		break;
	case BT_RedstoneTorchActive:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "redtorch_lit");
		break;
	case BT_StoneButton:
		break;
	case BT_Snow:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "snow");
		break;
	case BT_Ice:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "ice");
		break;
	case BT_SnowBlock:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "snow");
		break;
	case BT_Cactus:
		if(face == FACE_PositiveY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cactus_top");
		else if(face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cactus_bottom");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "cactus_side");	
		break;
	case BT_Clay:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "clay");
		break;
	case BT_SugarCane:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "reeds");
		break;
	case BT_Jukebox:
		if(face == FACE_PositiveY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "jukebox_top");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "musicBlock");
		break;
	case BT_Fence:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "wood");
		break;
	case BT_Pumpkin:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_top");
		else
		{
			if(block_data == BD_Pumpkin_Face_None)
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_side");
			}
			else if((block_data == BD_Pumpkin_Face_East && face == FACE_PositiveX) ||
				(block_data == BD_Pumpkin_Face_West && face == FACE_NegativeX) ||
				(block_data == BD_Pumpkin_Face_South && face == FACE_PositiveZ) ||
				(block_data == BD_Pumpkin_Face_North && face == FACE_NegativeZ))
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_face");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_side");
			}
		}
		break;
	case BT_Netherrack:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "hellrock");
		break;
	case BT_SoulSand:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "hellsand");
		break;
	case BT_Glowstone:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "lightgem");
		break;
	case BT_NetherPortal:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "portal");
		break;
	case BT_JackOLantern:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_top");
		else
		{
			if(block_data == BD_Pumpkin_Face_None)
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_side");
			}
			else if((block_data == BD_Pumpkin_Face_East && face == FACE_PositiveX) ||
				(block_data == BD_Pumpkin_Face_West && face == FACE_NegativeX) ||
				(block_data == BD_Pumpkin_Face_South && face == FACE_PositiveZ) ||
				(block_data == BD_Pumpkin_Face_North && face == FACE_NegativeZ))
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_jack");
			}
			else
			{
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "pumpkin_side");
			}
		}
		break;
	case BT_CakeBlock:
		break;
	case BT_RedstoneRepeaterInactive:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stone");
		break;
	case BT_RedstoneRepeaterActive:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stone");
		break;
	case BT_LockedChest:
		break;
	case BT_Trapdoor:
		break;
	case BT_MonsterEgg:
		break;
	case BT_StoneBricks:
		if(block_data == BD_StoneBrick_Normal)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebricksmooth");
		else if(block_data == BD_StoneBrick_Mossy)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebricksmooth_mossy");
		else if(block_data == BD_StoneBrick_Cracked)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebricksmooth_cracked");
		else if(block_data == BD_StoneBrick_Chiseled)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "stonebricksmooth_carved");
		break;
	case BT_HugeBrownMushroom:
		break;
	case BT_HugeRedMushroom:
		break;
	case BT_IronBars:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "fenceIron");
		break;
	case BT_GlassPane:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "glass");
		break;
	case BT_Melon:
		if(face == FACE_PositiveY || face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "melon_top");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "melon_side");
		break;
	case BT_PumpkinStem:
		break;
	case BT_MelonStem:
		break;
	case BT_Vines:
		break;
	case BT_FenceGate:
		break;
	case BT_BrickStairs:
		break;
	case BT_StoneBrickStairs:
		break;
	case BT_Mycelium:
		if(face == FACE_PositiveY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "mycel_top");
		else if(face == FACE_NegativeY)
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "dirt");
		else
			tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "mycel_side");
		break;
	case BT_LilyPad:
		break;
	case BT_NetherBrick:
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "netherBrick");
		break;
	case BT_NetherBrickFence:
		break;
	case BT_NetherBrickStairs:
		break;
	case BT_NetherWart:
		break;
	case BT_EnchantmentTable:
		break;
	case BT_BrewingStand:
		break;
	case BT_Cauldron:
		break;
	case BT_EndPortal:
		break;
	case BT_EndPortalBlock:
		break;
	case BT_EndStone:
		break;
	case BT_DragonEgg:
		break;
	case BT_RedstoneLampInactive:
		break;
	case BT_RedstoneLampActive:
		break;
	case BT_WoodenDoubleSlab:
		break;
	case BT_WoodenSlab:
		break;
	case BT_Cocoa:
		break;
	case BT_SandstoneStairs:
		break;
	case BT_EmeraldOre:
		break;
	case BT_EnderChest:
		break;
	case BT_TripwireHook:
		break;
	case BT_Tripwire:
		break;
	case BT_BlockOfEmerald:
		break;
	case BT_SpruceWoodStairs:
		break;
	case BT_BirchWoodStairs:
		break;
	case BT_JungleWoodStairs:
		break;
	case BT_CommandBlock:
		break;
	case BT_Beacon:
		break;
	case BT_CobblestoneWall:
		break;
	case BT_FlowerPot:
		break;
	case BT_Carrots:
		break;
	case BT_Potatoes:
		break;
	case BT_WoodenButton:
		break;
	case BT_MobHead:
		break;
	case BT_Anvil:
		break;
	case BT_TrappedChest:
		break;
	case BT_WeightedPressurePlateLight:
		break;
	case BT_WeightedPressurePlateHeavy:
		break;
	case BT_RedstoneComparatorInactive:
		break;
	case BT_RedstoneComparatorActive:
		break;
	case BT_DaylightSensor:
		break;
	case BT_BlockOfRedstone:
		break;
	case BT_NetherQuartzOre:
		break;
	case BT_Hopper:
		break;
	case BT_BlockOfQuarts:
		break;
	case BT_QuartStairs:
		break;
	case BT_ActivatorRail:
		break;
	case BT_Dropper:
		break;
	case BT_StainedClay:
		break;
	case BT_StainedGlassPane:
		break;
	case BT_LeavesAcaciaDarkOak:
		// TODO: No idea how to handle leaves...
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "leaves");
		break;
	case BT_WoodAcaciaDarkOak:
		// TODO: How to get the correct acacia texture? Also, i handle Acacia and Dark oak the same right now because I dont have separate textures for either of them.
		switch(block_data)
		{
			case BD_WoodAcacia_Normal:
			case BD_WoodDarkOak_Normal:
				if(face == FACE_PositiveY || face == FACE_NegativeY)
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_top");
					
				else
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_side");
				break;
			case BD_WoodAcacia_EastWest:
			case BD_WoodDarkOak_EastWest:
				if(face == FACE_NegativeX || face == FACE_PositiveX)
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_top");
				else
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_side");
				break;
			case BD_WoodAcacia_SouthNorth:
			case BD_WoodDarkOak_SouthNorth:
				if(face == FACE_NegativeZ || face == FACE_NegativeZ)
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_top");
				else
					tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_side");
				break;
			case BD_WoodAcacia_Directionless:
			case BD_WoodDarkOak_Directionless:
				tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "tree_side");
				break;
		}
		break;
	}

	if(tex == nullptr)
		tex = TextureManager::GetTexture(TextureManager::TEX_BLOCK, "blockGold");
	//ASSERT(tex != nullptr);

	return tex;*/
	return nullptr;
}

bool BlockIntersects(Ray &ray, const std::shared_ptr<Block> &block, BBoxFace &face, double &distance)
{
	BlockType type = block->type();
	int8_t data = block->data();
	BBox b(block->position(), (block->position() + Vector3d(1, 1, 1)));
	if(type == BT_Snow)
	{
		float block_height = ((data+1) / 8.0f);
		b.max_.Y() = block->position().Y() + block_height;
	}

	return b.Intersects(ray, face, distance);
}
