#ifndef DEFAULT_SURFACE_H_
#define DEFAULT_SURFACE_H_

#include <cstdint>
#include "vector.h"
#include <string>

struct FIBITMAP;

class BitmapSurface
{
private:
	uint32_t width_;
	uint32_t height_;
	FIBITMAP *bmp_;
	void InitBitmap();
public:
	BitmapSurface(uint32_t width, uint32_t height);
	uint32_t width() const { return width_; }
	uint32_t height() const { return height_; }
	void SetPixel(uint32_t x, uint32_t y, const Vector4d &color);
	void GetPixel(uint32_t x, uint32_t y, Vector4d &color);
	void save(const std::string &path);
};

#endif
