#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <cstdint>
#include <memory>
#include <vector>
#include <initializer_list>
#include "vector.h"

// forward declaration
class FIBITMAP;

class Texture
{
private:
	FIBITMAP *texture_;
	int32_t width_;
	int32_t height_;
	Vector2d texel_density_;

	static double Round(double x);
	static double IEEERemainder(double x, double y);
public:
	Texture(FIBITMAP *texture);
	~Texture();
	Texture *Clone() const;
	virtual void Sample(Vector4d &color, double u, double v, double mipmap_factor = 1) const;
	virtual void SampleBilinear(double u, double v, Vector4d &color) const;
	void SetPixel(int x, int y, const Vector4d &color, bool additive = false);
	int32_t width() const { return width_; }
	int32_t height() const { return height_; }
	FIBITMAP *bitmap() const { return texture_; }
};

typedef std::shared_ptr<Texture> TexturePtr;


class LODTexture : public Texture
{
private:
	std::vector<TexturePtr> levels_;
public:
	LODTexture(FIBITMAP *texture, std::vector<TexturePtr> &levels);
	LODTexture(std::initializer_list<FIBITMAP*> list);
};

typedef std::shared_ptr<LODTexture> LODTexturePtr;

#endif
