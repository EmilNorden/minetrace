#include "octreefactory.h"
#include "octree.h"
#include "blocktypes.h"
#include "region.h"
#include "nbt/nbt_tag.h"
#include "massert.h"
#include "binaryutil.h"
#include "world.h"
#include <thread>

inline bool is_solid_or_oob(boost::shared_array<int8_t> &blocks, int index)
{
	return index >= 0 && index < 4096 && static_cast<BlockType>(blocks[index]) != BT_Air && !block_traits[blocks[index]].has_texture_alpha;
}

int xyz_index(int x, int y, int z)
{
	return (y * 16 * 16) + (z * 16) + x;
}

class OctreeWorker
{
private:
	std::shared_ptr<Region> &region_;
	std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree_;
	int min_x_;
	int min_z_;
	Vector2i world_coordinate_;

	std::thread worker_thread_;

	void run_internal()
	{
		ASSERT(tree_.get() == 0);

		tree_.reset(new Octree<std::shared_ptr<Block>>);
		for(int chunk_x = min_x_; chunk_x < min_x_ + 16; ++chunk_x) /////////////////////// �NDRA TILL 16
		{
			for(int chunk_z = min_z_; chunk_z < min_z_ + 16; ++chunk_z)
			{
				int16_t index = BinaryUtil::Pack(static_cast<int8_t>(chunk_x), static_cast<int8_t>(chunk_z));

				auto chunk_it = region_->data_.find(index);
				if(chunk_it != region_->data_.end())
				{
					NBTTagPtr section_tag = chunk_it->second->Child("Level")->Child("Sections");
					std::vector<NBTPayload> sections = section_tag->payload_.get_data<std::vector<NBTPayload>>();
					for(auto it = sections.begin(); it != sections.end(); ++it)
					{
						std::vector<NBTTagPtr> section_data = it->get_data<std::vector<NBTTagPtr>>();
					
						int32_t section_y = -1;
						boost::shared_array<int8_t> blocks;
						boost::shared_array<int8_t> data;
						for(auto section_data_it = section_data.begin(); section_data_it != section_data.end(); ++section_data_it)
						{
							if((*section_data_it)->name() == "Y")
							{
								section_y = (*section_data_it)->payload_.get_data<int8_t>();
							}
							else if((*section_data_it)->name() == "Blocks")
							{
								blocks = (*section_data_it)->payload_.get_data<boost::shared_array<int8_t>>();
							}
							else if((*section_data_it)->name() == "Data")
							{
								data = (*section_data_it)->payload_.get_data<boost::shared_array<int8_t>>();
							}
						}
						ASSERT(section_y > -1);
						ASSERT(blocks.get() != nullptr);

						AddChunkSectionToTree(tree_, blocks, data, world_coordinate_.X() + (chunk_x * 16), section_y * 16, world_coordinate_.Y() + (chunk_z * 16));
					
						//for(auto section_data_it = section_data.begin(); section_data_it != section_data.end(); ++section_data_it)
						//{
						//	if((*section_data_it)->name() == "Blocks")
						//	{
						//		
						//		AddChunkSectionToTree(tree, blocks, region_world_coordinate.X() + (chunk_x * 16), section_y * 16, region_world_coordinate.Y() + (chunk_z * 16));
						//	}
						//	else if((*section_data_it)->name() == "Data")
						//	{
						//	}
						//}
					}
				}
			}
		}

		Vector3d octree_min_bound(world_coordinate_.X() + (min_x_ * 16), 0, world_coordinate_.Y() + (min_z_ * 16));
		Vector3d octree_max_bound = octree_min_bound + Vector3d(256, 256, 256);
		tree_->Build(8, BBox(octree_min_bound, octree_max_bound));
	}

	void AddChunkSectionToTree(std::shared_ptr<Octree<std::shared_ptr<Block>>> octree, boost::shared_array<int8_t> blocks, boost::shared_array<int8_t> data, int32_t x_offset, int32_t y_offset, int32_t z_offset)
	{
		for(int y = 0; y < 16; ++y)
		{
			for(int z = 0; z < 16; ++z)
			{
				for(int x = 0; x < 16; ++x)
				{
					//if(z == 0)
					//	octree->items_.push_back(Block(BT_Bricks, Vector3f(x + x_offset, y + y_offset, z + z_offset)));
					int index = xyz_index(x, y, z);


					//if(static_cast<BlockType>(blocks[index]) != BT_Air && 
					//	!is_solid_or_oob(blocks, xyz_index(x + 1, y, z)) && 
					//	!is_solid_or_oob(blocks, xyz_index(x - 1, y, z)) && 
					//	!is_solid_or_oob(blocks, xyz_index(x, y + 1, z)) && 
					//	!is_solid_or_oob(blocks, xyz_index(x, y - 1, z)) &&
					//	!is_solid_or_oob(blocks, xyz_index(x, y, z + 1)) &&
					//	!is_solid_or_oob(blocks, xyz_index(x, y, z - 1)))
					//{
					//	octree->items_.push_back(Block(static_cast<BlockType>(blocks[index]), Vector3f(x + x_offset, y + y_offset, z + z_offset)));
					//}

					if(static_cast<BlockType>(blocks[index]) != BT_Air &&
						(!is_solid_or_oob(blocks, xyz_index(x + 1, y, z)) || 
						!is_solid_or_oob(blocks, xyz_index(x - 1, y, z)) || 
						!is_solid_or_oob(blocks, xyz_index(x, y + 1, z)) || 
						!is_solid_or_oob(blocks, xyz_index(x, y - 1, z)) ||
						!is_solid_or_oob(blocks, xyz_index(x, y, z + 1)) ||
						!is_solid_or_oob(blocks, xyz_index(x, y, z - 1))))
					{
						if(static_cast<BlockType>(blocks[index]) != BT_Vines)
						{
						int8_t block_data;
						if(index % 2 == 0)
							block_data = data[index/2] & 0x0F;
						else
							block_data = (data[index/2] >> 4) & 0x0F;

						octree->items_.push_back(std::shared_ptr<Block>(new Block(static_cast<BlockType>(blocks[index]), block_data, Vector3d(x + x_offset, y + y_offset, z + z_offset))));
						}
					}
				}
			}
		}
	}

	void CreateFromRegionQuadrant(std::shared_ptr<Region> region, std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree, int min_x, int min_z, const Vector2i &region_world_coordinate)
	{
		
	}

public:
	OctreeWorker(std::shared_ptr<Region> &region, std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree,
		int min_x, int min_z, const Vector2i &world_coordinate)
		: region_(region), tree_(tree), min_x_(min_x), min_z_(min_z), world_coordinate_(world_coordinate)
	{
	}

	void run()
	{
		worker_thread_ = std::thread(&OctreeWorker::run_internal, this);
	}

	void block_until_finished()
	{
		worker_thread_.join();
	}
};

void OctreeFactory::CreateFromRegion(std::shared_ptr<Region> region,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree1,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree2,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree3,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree4)
{
	Vector2i world_coordinate = World::chunk_to_block_coord(World::region_to_chunk_coord(region->coordinate()));

	/*std::cout << "OctreeFactory: Creating octree from quadrant 1 of 4" << std::endl;
	CreateFromRegionQuadrant(region, tree1, 0, 0, world_coordinate);
	std::cout << "OctreeFactory: Creating octree from quadrant 2 of 4" << std::endl;
	CreateFromRegionQuadrant(region, tree2, 16, 0, world_coordinate);
	std::cout << "OctreeFactory: Creating octree from quadrant 3 of 4" << std::endl;
	CreateFromRegionQuadrant(region, tree3, 0, 16, world_coordinate);
	std::cout << "OctreeFactory: Creating octree from quadrant 4 of 4" << std::endl;
	CreateFromRegionQuadrant(region, tree4, 16, 16, world_coordinate);*/

	std::cout << "OctreeFactory: Creating octrees" << std::endl;
	OctreeWorker worker1(region, tree1, 0, 0, world_coordinate);
	OctreeWorker worker2(region, tree2, 16, 0, world_coordinate);
	OctreeWorker worker3(region, tree3, 0, 16, world_coordinate);
	OctreeWorker worker4(region, tree4, 16, 16, world_coordinate);

	worker1.run();
	worker2.run();
	worker3.run();
	worker4.run();

	worker1.block_until_finished();
	worker2.block_until_finished();
	worker3.block_until_finished();
	worker4.block_until_finished();
	std::cout << "OctreeFactory: Done creating octrees" << std::endl;
}
