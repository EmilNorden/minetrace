#ifndef RENDER_SURFACE_H_
#define RENDER_SURFACE_H_

#include <cstdint>
#include "vector.h"

template <typename TSurface>
class RenderTarget
{
private:
	TSurface surface_;
public:

	void SetPixel(uint32_t x, uint32_t y, const Vector4d &color); // Assuming 32bpp, should this be templated aswell?
	void GetPixel(uint32_t x, uint32_t y, Vector4d &color) const;

	void set_size(const Vector2ui &size);
	Vector2ui size() const;
};

template <typename TSurface>
void RenderTarget<TSurface>::set_size(const Vector<2, uint32_t> &size)
{
	surface_.set_size(size);
}

template <typename TSurface>
void RenderTarget<TSurface>::SetPixel(uint32_t x, uint32_t y, const Vector4d &color)
{
	surface_.SetPixel(x, y, p);
}

template <typename TSurface>
void RenderTarget<TSurface>::GetPixel(uint32_t x, uint32_t y, Vector4d &color) const
{
	surface_.GetPixel(x, y, color);
}

#endif