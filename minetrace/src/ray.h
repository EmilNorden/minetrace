#ifndef RAY_H_
#define RAY_H_

#include "vector.h"
#include <cfloat>

class Ray
{
private:

public:
#ifdef DEBUG_RAY_METRICS
	int intersection_checks_;
#endif

	Ray(const Vector3d &origin, const Vector3d &direction)
		: origin_(origin), direction_(direction), distance_(DBL_MAX), depth_(0)
	{
#ifdef DEBUG_RAY_METRICS
		intersection_checks_ = 0;
#endif
	}

	Ray()
		: distance_(DBL_MAX), depth_(0)
	{
#ifdef DEBUG_RAY_METRICS
		intersection_checks_ = 0;
#endif
	}


	
	Vector3d origin_;
	Vector3d direction_;
	double distance_;
	int depth_;
	Vector3d origin() const { return origin_; }
	Vector3d direction() const { return direction_; }
	double distance() const { return distance_; }
};

#endif
