#include "raytracer.h"
#include "assert.h"
#include "camera.h"
#include "block_intersection_handler.h"
#include "texture.h"

RayTracer::RayTracer(const RayTracerSettings &settings)
        : running_(false), num_threads_(1), random_(new std::mt19937(time(0))), settings_(settings)
{
	initialize_surface();
}

void RayTracer::initialize_surface()
{
	surface_ = std::make_shared<BitmapSurface>(settings_.horizontal_res, settings_.vertical_res);
}

void RayTracer::render_async()
{
        do
        {
                int current_scanline = ++scanline_;
                if(current_scanline >= settings_.vertical_res)
                        break;

                for(uint32_t x = 0; x < settings_.horizontal_res; ++x)
                {
                        Ray r;
                        camera_->cast_perturbed_ray(r, x, current_scanline, settings_.dof_radius, random_);

                        Vector4d sample_color = trace_ray(r);
                        Vector4d &pixel_color = back_buffer_[(current_scanline * settings_.horizontal_res) + x];
                        pixel_color *= current_sample_;
                        pixel_color += sample_color;
                        pixel_color *= (1.0 / (current_sample_ + 1));
                }

        }while(scanline_ < 1024);
}


void RayTracer::render()
{
        ASSERT(!running_);

        back_buffer_ = new Vector4d[settings_.horizontal_res * settings_.vertical_res];
        //std::unique_ptr<Vector4d[]> buffer(new Vector4d[width*height]);

        //std::shared_ptr<std::mt19937> ;

        auto p = std::bind(&RayTracer::render_async, this);
        std::cout << "Rendering..." << std::endl;
        {
                stopwatch watch("Rendering");
                for(current_sample_ = 0; current_sample_ < settings_.samples; ++current_sample_)
                {
                        scanline_.store(-1);
                        std::thread threads[1];

                        for(uint32_t i = 0; i < 1; ++i)
                                threads[i] = std::thread(p);

                        for(uint32_t i = 0; i < 1; ++i)
                                threads[i].join();
                }
        }


        for(uint32_t y = 0; y < settings_.vertical_res; ++y)
        {
                for(uint32_t x = 0; x < settings_.horizontal_res; ++x)
                {
                        surface_->SetPixel(settings_.horizontal_res - x - 1, y, back_buffer_[(y * settings_.vertical_res) + x]);
                }
        }

        delete back_buffer_;
}


Vector4d RayTracer::trace_ray(Ray &ray)
{
        IntersectionInfo<std::shared_ptr<Block>> intersection;
        Vector4d color;
        if(world_->CastRay(ray, intersection))
        {
		shade(ray, intersection, color);
//                shade(ray, intersection, world_, textures_, settings_, color);
        }
        else
        {
  //              shade_sky(color);
        }
        return color;
}

void RayTracer::shade(Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, Vector4d &color)
{
	color.X() = 0;
	color.Y() = 0;
	color.Z() = 0;
	color.W() = 0;
	
	if(ray.depth_ > settings_.max_depth)
		return;

	LODTexturePtr texture = get_block_texture(ray, intersection, textures_);
	
	// sample emissive color
	// color += emissive

	// sample diffuse color
	Vector4d diffuse;
	texture->Sample(diffuse, intersection.u, intersection.v);
		

	color += diffuse;
}
