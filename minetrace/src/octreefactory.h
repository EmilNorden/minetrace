#ifndef OCTREE_FACTORY_H_
#define OCTREE_FACTORY_H_

#include <cstdint>
#include <memory>
#include <boost/shared_array.hpp>
#include "octree.h"
#include "block.h"

class Region;

class OctreeFactory
{
public:
	static void CreateFromRegion(std::shared_ptr<Region> region,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree1,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree2,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree3,
		std::shared_ptr<Octree<std::shared_ptr<Block>>> &tree4);
};

#endif