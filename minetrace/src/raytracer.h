#ifndef RAY_TRACER_H_
#define RAY_TRACER_H_

#include <random>
#include <ctime>
#include <memory>
#include <cstdint>
#include "intersection_info.h"
#include "vector.h"
#include "camera.h"
#include "ray.h"
#include "world.h"
#include "massert.h"	
#include "block_intersection_handler.h"
#include "texture.h"
#include "output_helper.h"
#include "util.h"
#include <atomic>
#include <thread>
#include "bitmap_surface.h"

class TextureHandler;

struct RayTracerSettings
{
public:
	uint32_t horizontal_res;
	uint32_t vertical_res;
	uint32_t samples;
	uint32_t max_depth;
	double dof_radius;
};

class RayTracer
{
private:
	std::shared_ptr<Camera> camera_;
	std::shared_ptr<World> world_;
	std::shared_ptr<BitmapSurface> surface_;
	std::shared_ptr<TextureHandler> textures_;
	std::shared_ptr<std::mt19937> random_;
	RayTracerSettings settings_;
	bool running_;
	uint32_t num_threads_;
	uint32_t current_sample_;
	std::atomic_int scanline_;
	Vector4d *back_buffer_;

	Vector4d trace_ray(Ray &ray);

	void initialize_surface();
	void render_async();
	void shade(Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, Vector4d &color); 
public:
	std::shared_ptr<Camera> camera() const { return camera_; }
	void set_camera(std::shared_ptr<Camera> camera) { camera_ = camera; }
	std::shared_ptr<World> world() const { return world_; }
	void set_world(std::shared_ptr<World> world) { world_ = world; }
	std::shared_ptr<TextureHandler> textures() const { return textures_; }
	void set_textures(const std::shared_ptr<TextureHandler> textures) { textures_ = textures; }

	RayTracer(const RayTracerSettings &settings);
	void render();

	std::shared_ptr<BitmapSurface> &surface() { return surface_; }
};

#endif
