#ifndef BLOCK_FACE_H_
#define BLOCK_FACE_H_

enum BBoxFace
{
	FACE_PositiveX,
	FACE_NegativeX,
	FACE_PositiveY,
	FACE_NegativeY,
	FACE_PositiveZ,
	FACE_NegativeZ
};

#endif