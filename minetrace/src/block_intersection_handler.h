#ifndef BLOCK_INTERSECTION_HANDLER
#define BLOCK_INTERSECTION_HANDLER

#include "intersection_info.h"
#include "block.h"
#include <memory>
#include "texture.h"

class Ray;
class TextureHandler;

const Texture *HandleBlockIntersection(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection);

LODTexturePtr get_block_texture(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<TextureHandler> &textures);

bool BlockIntersects(Ray &ray, const std::shared_ptr<Block> &block, BBoxFace &face, double &distance); 

#endif
