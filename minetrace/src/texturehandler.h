#ifndef TEXTUREHANDLER_H_
#define TEXTUREHANDLER_H_

#include <map>
#include <string>
#include "texture.h"

class TextureHandler
{
private:
	std::map<std::string, LODTexturePtr> textures_;
	std::string minecraft_path_;
public:
	TextureHandler(const std::string &minecraft_path);

	void Initialize();
	void LoadMipMap(const Texture *level0, Texture *level1, Texture *level2, Texture *level3, Texture level4);
	LODTexturePtr LoadMipMap(const Texture *levels[5], int x, int y);

	LODTexturePtr &GetTexture(const std::string &name);
};



#endif
