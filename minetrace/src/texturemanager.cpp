#include "texturemanager.h"
#include "blocktypes.h"
#include "freeimage/FreeImage.h"
#include <string>
#include "texture.h"
#include "massert.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

std::map<std::string, Texture*> TextureManager::block_textures_;
std::map<std::string, Texture*> TextureManager::item_textures_;

void TextureManager::Initialize()
{
	boost::filesystem::path base_dir("C:\\Users\\Emil\\minecraft\\");
	boost::filesystem::directory_iterator end_it;

	boost::filesystem::path block_dir(base_dir);
	block_dir /= "textures"; // Yes, /= actually appends a folder to the path...
	block_dir /= "blocks";

	boost::filesystem::path item_dir(base_dir);
	item_dir /= "item";

	boost::filesystem::path item_chest_dir(item_dir);
	item_chest_dir /= "chests";

	LoadTextures(block_textures_, block_dir);
	LoadTextures(item_textures_, item_dir);
	LoadTextures(item_textures_, item_chest_dir);
}

const Texture *TextureManager::GetTexture(TextureManager::TextureType type, const std::string &name)
{
	switch(type)
	{
	case TEX_BLOCK:
		return GetTextureInternal(block_textures_, name);
		break;
	case TEX_ITEM:
		return GetTextureInternal(item_textures_, name);
		break;
	}
	return nullptr;
}

const Texture *TextureManager::GetTextureInternal(const std::map<std::string, Texture*> &texture_map, const std::string &name)
{
	auto it = texture_map.find(name);
	if(it == texture_map.end())
		return nullptr;
	return it->second;
}

void TextureManager::Finalize()
{
	auto texture_iterator = block_textures_.begin();
	while(texture_iterator != block_textures_.end())
	{
		delete texture_iterator->second;
		texture_iterator = block_textures_.erase(texture_iterator);
	}

	texture_iterator = item_textures_.begin();
	while(texture_iterator != item_textures_.end())
	{
		delete texture_iterator->second;
		texture_iterator = item_textures_.erase(texture_iterator);
	}
}

void TextureManager::LoadTextures(std::map<std::string, Texture*> &texture_map, boost::filesystem::path& folder)
{
	boost::filesystem::directory_iterator end_it;
	for(boost::filesystem::directory_iterator it(folder);  it != end_it; ++it)
	{
		const boost::filesystem::path file = it->path();
		if(boost::iequals(file.extension().string(), ".png"))
		{
			std::string texture_name = file.filename().replace_extension("").string();
			FIBITMAP *bitmap = FreeImage_Load(FIF_PNG, file.string().c_str());
			ASSERT(bitmap != 0);
			texture_map.insert(std::pair<std::string, Texture*>(texture_name, new Texture(bitmap)));
		}
	}
}

Texture *TextureManager::CreateTinted(const Texture *texture, const Vector4d &tint)
{
	ASSERT(texture != nullptr);

	Texture *cloned_texture = texture->Clone();
	ASSERT(cloned_texture != nullptr);

	for(int y = 0; y < texture->height(); ++y)
	{
		for(int x = 0; x < texture->width(); ++x)
		{
			cloned_texture->SetPixel(x, y, tint, true);
		}
	}

	return cloned_texture;
}

void TextureManager::RegisterTexture(Texture *texture, TextureType type, const std::string &name)
{
	ASSERT(texture != nullptr);
	ASSERT(!name.empty());

	if(type == TEX_BLOCK)
		block_textures_.insert(std::pair<std::string, Texture*>(name, texture));
	else if(type == TEX_ITEM)
		item_textures_.insert(std::pair<std::string, Texture*>(name, texture));
}
