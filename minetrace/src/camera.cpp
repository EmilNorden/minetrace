#include "camera.h"
#define PI 3.14159265359

void Camera::calculate_n()
{
	n_ = direction_ * -1;
	n_.Normalize();
}

void Camera::calculate_uv()
{
	u_ = up_.Cross(n_);
	u_.Normalize();

	v_ = n_.Cross(u_);
	v_.Normalize();

}

void Camera::update() {
	const int distance = 10;

	image_plane_height_ = 2 * distance * tan(fov_ / 2.0);
	image_plane_width_ = image_plane_height_ * aspect_ratio_;

	calculate_n();
	calculate_uv();

	Vector3d image_plane_center = position_ - n_ * distance;

	image_plane_start_ = image_plane_center + (u_ * (image_plane_width_ / 2.0)) - (v_ * (image_plane_height_ / 2.0));

	pixel_width_ = image_plane_width_ / resolution_.X();
	pixel_height_ = image_plane_height_ / resolution_.Y();
	updated_this_frame_ = true;
}

//void Camera::cast_ray(Ray *ray, int x, int y) const {
//	ray->origin_ = position_;
//
//	ray->direction_ = (image_plane_start_ - (u_ * pixel_width_ * (double)x) + (v_ * pixel_height_ * (double)y)) - position_;
//	ray->direction_.normalize_device();
//}

void Camera::cast_ray(Ray &ray, int x, int y) const {
	ray.origin_ = position_;

	ray.direction_ = (image_plane_start_ - (u_ * pixel_width_ * (double)x) + (v_ * pixel_height_ * (double)y)) - position_;
	ray.direction_.Normalize();
}

void Camera::cast_perturbed_ray(Ray &ray, int x, int y, double radius, std::shared_ptr<std::mt19937> &mt_rand) const
{
	cast_ray(ray, x, y);

	Vector3d focus_point = position_ + ray.direction_ * focal_length_;

	double min = (double)mt_rand->min();
	double max = (double)mt_rand->max();
	double range = max - min;

	double u_shift = (((double)mt_rand->operator()() - min) / range); //(MathUtil::get_rand()  * radius) - (radius / 2.0);
	double v_shift = (((double)mt_rand->operator()() - min) / range); //(MathUtil::get_rand()  * radius) - (radius / 2.0);

	double r = radius;

	ray.origin_ = position_ - (u_ * (r/2)) - (v_ * (r/2)) + (u_ * r * u_shift) + (v_ * r * v_shift);
	ray.direction_ = focus_point - ray.origin_;
	ray.direction_.Normalize();
}

//void Camera::cast_perturbed_ray(Ray &ray, int x, int y, curandState &rand_state) const
//{
//	cast_ray(ray, x, y);
//
//	Vector3d focus_point = position_ + ray.direction_ * focal_length_;
//
//	double angle = curand_uniform(&rand_state) * PI * 2;
//	double length = curand_uniform(&rand_state) * blur_radius_;
//
//	ray.origin_ = position_ + (u_ * sin(angle) * length) + (v_ * cos(angle) * length);
//	ray.direction_ = focus_point - ray.origin_;
//	ray.direction_.normalize_device();
//
//	// This created a rectangular blur
//	/*double u_shift = curand_uniform(&rand_state);
//	double v_shift = curand_uniform(&rand_state); 
//
//	double r = radius;
//
//	ray.origin_ = position_ - (u_ * (r/2)) - (v_ * (r/2)) + (u_ * r * u_shift) + (v_ * r * v_shift);
//	ray.direction_ = focus_point - ray.origin_;
//	ray.direction_.normalize();*/
//}

void Camera::reset_update_flag()
{
	updated_this_frame_ = false;
}