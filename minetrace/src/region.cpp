#include "region.h"
#include "binaryreader.h"
#include "nbt/nbt_parser.h"
#include <fstream>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/copy.hpp>
#include <vector>
#include <sstream>
#include <string>

#include <iostream>

namespace io = boost::iostreams;

Region::Region()
	: loaded_(false)
{
}

Region::~Region()
{
	std::cout << "Region [" << coordinate_.X() << ", " << coordinate_.Y() << "] destroyed" << std::endl;
}

void Region::LoadFile(const std::string &folder, int x, int z)
{
	coordinate_.X() = x;
	coordinate_.Y() = z;

	std::stringstream ss;
	ss << folder;
	
	if(boost::algorithm::ends_with(folder, "\\") || boost::algorithm::ends_with(folder, "/"))
		ss << '/';
	ss << "r." << x << "." << z << ".mca";
	LoadFile(ss.str());	
}

void Region::LoadFile(const std::string &full_path)
{
	std::ifstream file;

	file.open(full_path, std::ios::binary | std::ios::in);
	if(file.is_open())
	{
		BinaryReader<true> reader(file);

		boost::shared_array<int32_t> locations = reader.ReadArray<int32_t>(1024);
		boost::shared_array<int32_t> timestamps = reader.ReadArray<int32_t>(1024);

		for(int x = 0; x < 32; ++x)
		{
			for(int z = 0; z < 32; ++z)
			{
				int32_t offset = (x + z * 32);

				int32_t pos = locations[offset] >> 8;
				int8_t sector_count = locations[offset] & 0x000000FF;

				locations_.insert(std::pair<int16_t, int32_t>((x << 8) | z, locations[offset]));
				timestamps_.insert(std::pair<int16_t, int32_t>((x << 8) | z, timestamps[offset]));
				
				if(pos || sector_count)
				{
					reader.SetOffset(pos * 4096);

					int32_t data_length = reader.ReadInt32();
					int8_t compression = reader.ReadByte();

					// Read NBT data
					boost::shared_array<int8_t> data = reader.ReadArray<int8_t>(data_length - 1);
					
					io::array_source compressedArray( reinterpret_cast< const char* >( &data[0] ), data_length - 1 );
					io::filtering_streambuf<io::input> in;
					io::zlib_decompressor decompressor;
					in.push(decompressor);
					in.push(compressedArray);

					std::vector<char> decompressed;
					io::copy(in, io::back_inserter(decompressed));
					io::stream<io::basic_array<char>> s(io::basic_array<char>(decompressed.data(), decompressed.size()));

					NBTParser parser;
					data_.insert(std::pair<int16_t, NBTTagPtr>((x << 8) | z, parser.Parse(s)));

				}
			}
		}
		
		file.close();
		loaded_ = true;
	}
	
}
