#include "camera.h"
#include "world.h"
#include <memory>
#include "bitmap_surface.h"

#include <iostream>

#include "freeimage/FreeImage.h"
#include "texturemanager.h"
#include "texturehandler.h"
#include "raytracer.h"
#include <cmath>

#include <cstdlib>
#include <ctime>

#define RENDER_WIDTH 1024
#define RENDER_HEIGHT 1024
#define PI 3.14159265359

double deg_to_rad(double deg)
{
	return deg * (180.0 / PI);
}


void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message) {
	printf("\n*** "); 
	if(fif != FIF_UNKNOWN) {
		printf("%s Format\n", FreeImage_GetFormatFromFIF(fif));
	}
	printf(message);
	printf(" ***\n");
}

double pi() { return std::atan(1.0)*4; }

int main(int argc,  char **argv)
{	
	FreeImage_Initialise();
	FreeImage_SetOutputMessage(FreeImageErrorHandler);

	//Camera2 c;
	//c.position_ = Vector3f(0, 0, 0);
	//c.target_ = Vector3f(0, 4, 10);
	//c.up_ = Vector3f(0, 1, 0);
	//c.init();

	std::shared_ptr<TextureHandler> textures = std::make_shared<TextureHandler>("/home/emil/.minecraft");
	textures->Initialize();

	Vector3d cam_pos(243, 68, 246);
	Vector3d cam_target(Vector3d(256, 65, 226) - cam_pos);
	/*Vector3d cam_pos(-468, 76, -784.9);
	Vector3d cam_target(Vector3d(-465.7, 76, -783) - cam_pos);*/
	/*Vector3d cam_pos(48, 76, -837.3);
	Vector3d cam_target(Vector3d(46.78, 77.3, -839.834) - cam_pos);*/
	/*Vector3d cam_pos(8, 8, 50);
	Vector3d cam_target(0, 0, -1);*/
	cam_target.Normalize();

	Vector3d focus_point(-250, 70, 253);
	double focal_length = (focus_point - cam_pos).Length();
	std::shared_ptr<Camera> camera(new Camera(cam_pos, cam_target, Vector3d(0, 1, 0), deg_to_rad(70), RENDER_WIDTH / (double)RENDER_HEIGHT, Vector2i(RENDER_WIDTH, RENDER_HEIGHT), focal_length)); // PI / 4.0
	std::shared_ptr<World> world(new World);
	world->set_camera(camera);
	
	world->set_path("/home/emil/mc/server3/world/region/");
	
	world->LoadRegions();
	std::cout << "World size: " << world->block_count() << " blocks"<< std::endl;

	RayTracerSettings settings;
	settings.horizontal_res = RENDER_WIDTH;
	settings.vertical_res = RENDER_HEIGHT;
	settings.samples = 1;
	settings.dof_radius = 0.0;

	RayTracer tracer(settings);

	tracer.set_camera(camera);
	tracer.set_world(world);
	tracer.set_textures(textures);
	camera->update();
	tracer.render();
	tracer.surface()->save("/home/emil/Pictures/mc/mc2.png");
	std::cout << "Done" << std::endl;

	TextureManager::Finalize();
	FreeImage_DeInitialise();

	return 0;
}
