/// This code is not pretty. Dont look.

#ifndef BINARY_READER_H_
#define BINARY_READER_H_

#include <iosfwd>
#include <cstdint>
#include <string>

#include "binaryutil.h"
#include "boost/shared_array.hpp"
#include "boost/noncopyable.hpp"

template<typename T>
struct binarystream_array_deleter
{
   void operator()(T* p)
   {
      delete [] p;
   }
};

template <bool _BigEndian>
class BinaryReader : boost::noncopyable
{
private:
	std::istream &stream_;
public:
	BinaryReader(std::istream &stream)
		: stream_(stream)
	{
	}

	void SetOffset(int64_t pos) { stream_.seekg(pos, std::ios::beg); }

	int8_t ReadByte();
	int16_t ReadInt16();
	int32_t ReadInt32();
	int64_t ReadInt64();
	float ReadFloat();
	double ReadDouble();
	std::string ReadString();

	template<typename T>
	boost::shared_array<T> ReadArray(size_t count);

};

template <>
template <typename T>
boost::shared_array<T> BinaryReader<false>::ReadArray(size_t count)
{
	T* result = new T[count];
	stream_.read(reinterpret_cast<char*>(result), sizeof(T) * count);
	
	return boost::shared_array<T>(result);
}

template <>
template <typename T>
boost::shared_array<T> BinaryReader<true>::ReadArray(size_t count)
{
	T* result = new T[count];
	stream_.read(reinterpret_cast<char*>(result), sizeof(T) * count);

	for(size_t i = 0; i < count; ++i)
		result[i] = BinaryUtil::SwapEndianness<T>(result[i]);
	
	return boost::shared_array<T>(result);
}

template <bool _BigEndian>
int8_t BinaryReader<_BigEndian>::ReadByte()
{
	int8_t c;
	stream_.read(reinterpret_cast<char*>(&c), 1);

	return c;
}



#endif