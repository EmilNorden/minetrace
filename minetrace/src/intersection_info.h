#ifndef INTERSECTIONINFO_H_
#define INTERSECTIONINFO_H_

#include "block_face.h"
#include "vector.h"

template<typename T>
class IntersectionInfo
{
private:

	int cuboid_id_;
public:
	T object;
	Vector3d normal;
	Vector3d coordinate;
	double u, v; 
	BBoxFace face;
	IntersectionInfo() { }
};


#endif