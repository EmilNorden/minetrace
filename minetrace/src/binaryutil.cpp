#include "binaryutil.h"

template <typename TWide, typename T>
TWide PackInternal(T a, T b)
{
	// I feel it should be simpler, but I could not get it to work. for instance, right binary shift 32 bits seems to result in UD.
	// However, most of these are constant values that can be resolved at compile time, so it is not as bad as it seems.
	return (static_cast<TWide>(a) << (sizeof(TWide) * 4)) | (b & ~(static_cast<TWide>(~0) << (sizeof(TWide) * 4)));
}

template <typename TWide, typename T>
void UnpackInternal(TWide in, T &a, T &b)
{
	a = (in & static_cast<TWide>(static_cast<T>(-1))) >> sizeof(TWide) * 4;
	b = in & static_cast<T>(-1);
}

int64_t BinaryUtil::Pack(int32_t a, int32_t b)
{
	return PackInternal<int64_t, int32_t>(a, b);
}

void BinaryUtil::Unpack(int64_t in, int32_t& a, int32_t& b)
{
	UnpackInternal<int64_t, int32_t>(in, a, b);
}

int32_t BinaryUtil::Pack(int16_t a, int16_t b)
{
	return PackInternal<int32_t, int16_t>(a, b);
}

void BinaryUtil::Unpack(int32_t in, int16_t& a, int16_t& b)
{
	UnpackInternal<int32_t, int16_t>(in, a, b);
}

int16_t BinaryUtil::Pack(int8_t a, int8_t b)
{
	return PackInternal<int16_t, int8_t>(a, b);
}

void BinaryUtil::Unpack(int16_t in, int8_t& a, int8_t& b)
{
	UnpackInternal<int16_t, int8_t>(in, a, b);
}

//template<>
//inline
//static int8_t BinaryUtil::SwapEndianness<int8_t>(int8_t in)
//{
//	return ((in & 0x00FF) << 8) | ((in & 0xFF00) >> 8);
//}
//
//template<>
//inline
//static int16_t BinaryUtil::SwapEndianness<int16_t>(int16_t in)
//{
//	return ((in & 0x00FF) << 8) | ((in & 0xFF00) >> 8);
//}
//
//template<>
//inline
//static int32_t BinaryUtil::SwapEndianness<int32_t>(int32_t in)
//{
//	return ((in & 0x0000FF00) << 8) | ((in & 0x00FF0000) >> 8) |
//			((in & 0x000000FF) << 24) | ((in & 0xFF000000) >> 24);
//}
//
//template<>
//inline
//static int64_t BinaryUtil::SwapEndianness<int64_t>(int64_t in)
//{
//	return ((in & 0x00000000000000FF) << 56) | ((in & 0xFF00000000000000) >> 56) | 
//			((in & 0x000000000000FF00) << 40) | ((in & 0x00FF000000000000) >> 40) | 
//			((in & 0x0000000000FF0000) << 24) | ((in & 0x0000FF0000000000) >> 24) |
//			((in & 0x00000000FF000000) << 8) | ((in & 0x000000FF00000000) >> 8);
//}