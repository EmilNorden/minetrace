#include "massert.h"
#include <iostream>

void assert_failed(const char *file, int line)
{
	std::cerr << "Assertion failed - " << file << ":" << line << std::endl << static_cast<char>(7);
	exit(1);
}
