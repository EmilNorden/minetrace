#include "binaryreader.h"

template <>
int16_t BinaryReader<false>::ReadInt16()
{
	int16_t i;
	stream_.read(reinterpret_cast<char*>(&i), 2);

	return i;
}

template <>
int16_t BinaryReader<true>::ReadInt16()
{
	int16_t i;
	stream_.read(reinterpret_cast<char*>(&i), 2);
		
	return BinaryUtil::SwapEndianness(i);
}

template <>
int32_t BinaryReader<false>::ReadInt32()
{
	int32_t i;
	stream_.read(reinterpret_cast<char*>(&i), 4);

	return i;
}

template <>
int32_t BinaryReader<true>::ReadInt32()
{
	int32_t i;
	stream_.read(reinterpret_cast<char*>(&i), 4);
		
	return BinaryUtil::SwapEndianness(i);
}

template <>
int64_t BinaryReader<false>::ReadInt64()
{
	int64_t i;
	stream_.read(reinterpret_cast<char*>(&i), 8);

	return i;
}

template <>
int64_t BinaryReader<true>::ReadInt64()
{
	int64_t i;
	stream_.read(reinterpret_cast<char*>(&i), 8);
		
	return BinaryUtil::SwapEndianness(i);
}

template <>
std::string BinaryReader<false>::ReadString()
{
	int16_t stringLength;
	stream_.read(reinterpret_cast<char*>(&stringLength), 2);

	char *buffer = new char[stringLength];
	stream_.read(buffer, stringLength);

	std::string result(buffer, (size_t)stringLength);

	delete[] buffer;

	return result;
}

template <>
float BinaryReader<false>::ReadFloat()
{
	float f;
	stream_.read(reinterpret_cast<char*>(&f), 4);

	return f;
}

template <>
float BinaryReader<true>::ReadFloat()
{
	float f;
	stream_.read(reinterpret_cast<char*>(&f), 4);

	return BinaryUtil::SwapEndianness(f);
}

template <>
double BinaryReader<false>::ReadDouble()
{
	double d;
	stream_.read(reinterpret_cast<char*>(&d), 8);

	return d;
}

template <>
double BinaryReader<true>::ReadDouble()
{
	double d;
	stream_.read(reinterpret_cast<char*>(&d), 8);

	return BinaryUtil::SwapEndianness(d);
}

template <>
std::string BinaryReader<true>::ReadString()
{
	int16_t stringLength;
	stream_.read(reinterpret_cast<char*>(&stringLength), 2);

	stringLength = BinaryUtil::SwapEndianness(stringLength);

	char *buffer = new char[stringLength];
	stream_.read(buffer, stringLength);

	std::string result(buffer, (size_t)stringLength);

	delete[] buffer;

	return result;
}
