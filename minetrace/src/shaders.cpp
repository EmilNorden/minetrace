#include "shaders.h"
#include "texturemanager.h"
#include "texture.h"
#include "block_intersection_handler.h"
#include "blocktypes.h"
#include "ray.h"
#include "texturehandler.h"

void light(const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<World> &world, const std::shared_ptr<TextureHandler> &textures, Vector4d &out_color)
{
	/*Vector3f light_dir(-1, 1, 0);
	light_dir.Normalize();
	float dot = Vector3f::Dot(light_dir, intersection.normal);
	if(dot < 0.2f)
		dot = 0.2f;
	out_color.X() *= dot;
	out_color.Y() *= dot;
	out_color.Z() *= dot;*/

	Vector3d light_pos(-1200.0, 1041.334f, 500.48f);

	Vector3d ld = light_pos - intersection.coordinate;
	double light_distance = Vector3d::Distance(light_pos, intersection.coordinate);
	ld.Normalize();
	Vector3d ld_f = Vector3d(ld.X(), ld.Y(), ld.Z());
	double dot = Vector3d::Dot(ld_f, intersection.normal);

	Ray shadow_ray;
	shadow_ray.origin_ = intersection.coordinate;
	shadow_ray.direction_ = ld;
	shadow_ray.distance_ = light_distance;
	IntersectionInfo<std::shared_ptr<Block>> shadow_intersection;

	intersection.object->set_ignore_collisions(true);
	double light = dot;
	if(world->CastRay(shadow_ray, shadow_intersection))
	{
		Vector4d light_color;
	//	shade(shadow_ray, shadow_intersection, world, textures, light_color);
		
		light = dot - light_color.W();
	}

	intersection.object->set_ignore_collisions(false);

	out_color.X() *= light;
	out_color.Y() *= light;
	out_color.Z() *= light;
}

void standard_shade(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<World> &world, const std::shared_ptr<TextureHandler> &textures, Vector4d &out_color)
{
	LODTexturePtr texptr = get_block_texture(ray, intersection, textures);
	texptr->Sample(out_color, intersection.u, intersection.v);
	{
		light(intersection, world, textures, out_color);
	}
}

void shade(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<World> &world, const std::shared_ptr<TextureHandler> &textures, const RayTracerSettings &settings, Vector4d &out_color)
{
	Vector3d color(0, 0, 0);
	
	
	Vector4d diffuse;

	//const Texture *tex = HandleBlockIntersection(ray, intersection);
	//tex->Sample(intersection.u, intersection.v, out_color);

	// color += emissive HACK below
	if(intersection.object->type() == BT_Lava)
		color += Vector3d(1, 1, 1);

	//if(diffuse_color

	standard_shade(ray, intersection, world, textures, out_color);
}

void shade_sky(Vector4d &out_color)
{
	out_color.X() = 0;
	out_color.Y() = 62.0f / 255.0f;
	out_color.Z() = 232.0f / 255.0f;

	out_color += Vector4d(1, 1, 1, 1) * 0.5;

	if(out_color.X() > 1)
		out_color.X() = 1;
	if(out_color.Y() > 1)
		out_color.Y() = 1;
	if(out_color.Z() > 1)
		out_color.Z() = 1;

}
