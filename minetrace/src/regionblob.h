#ifndef REGION_BLOB_H_
#define REGION_BLOB_H_

#include <cstdint>
#include <boost/shared_array.hpp>
#include <string>
#include <memory>

class RegionBlob
{
private:
	RegionBlob();
public:
	int32_t locations[1024];
	int32_t timestamps[1024];
	boost::shared_array<int8_t> data;

	static std::shared_ptr<RegionBlob> LoadFile(const std::string &path);
}

#endif