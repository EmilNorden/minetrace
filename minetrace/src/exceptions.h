#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <exception>
#include <string>

class ArgumentException : public std::exception
{
private:
	std::string argument_;
	std::string message_;
public:
	ArgumentException(const std::string &arg, const std::string &msg)
		: argument_(arg), message_(msg)
	{
	}

	const char *what() const noexcept
	{
		std::string message = std::string("Invalid argument '") + argument_ + "': " + message_;
		return message.c_str();
	}
};

class InvalidOperationException : public std::exception
{
private:
	std::string message_;
public:
	InvalidOperationException(const std::string &msg)
		: message_(msg)
	{
	}

	const char *what() const noexcept
	{
		return message_.c_str();
	}
};

#endif
