#ifndef ASSERT_H_
#define ASSERT_H_
#define ENABLE_ASSERT

void assert_failed(const char *file, int line);

#ifdef ENABLE_ASSERT
#define ASSERT(x)  { if(!(x)) { assert_failed(__FILE__, __LINE__); } }
#else
#define ASSERT(x)
#endif




#endif