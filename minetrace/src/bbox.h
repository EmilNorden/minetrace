#ifndef BBOX_H_
#define BBOX_H_

#include "vector.h"
#include "ray.h"
#include "block_face.h"
#include <cmath>
#include <iostream>

#define MAX(a, b) a > b ? a : b
#define MIN(a, b) a > b ? b : a
#define INTERSECT_EPSILON 9.99999997475243E-07

class BBox
{
public:
	Vector3d min_;
	Vector3d max_;

	BBox()
	{
	}

	BBox(const Vector3d &min, const Vector3d &max)
		: min_(min), max_(max)
	{}

	BBox(const BBox &other)
		: min_(other.min_), max_(other.max_)
	{
	}

	bool Contains(const Vector3d &point) const
	{
		return (point.X() >= min_.X() && point.X() < max_.X() &&
			point.Y() >= min_.Y() && point.Y() < max_.Y() &&
			point.Z() >= min_.Z() && point.Z() < max_.Z());
	}

	void Expand(const BBox &other)
	{
		this->max_.X() = MAX(this->max_.X(), other.max_.X());
		this->max_.Y() = MAX(this->max_.Y(), other.max_.Y());
		this->max_.Z() = MAX(this->max_.Z(), other.max_.Z());

		this->min_.X() = MIN(this->min_.X(), other.min_.X());
		this->min_.Y() = MIN(this->min_.Y(), other.min_.Y());
		this->min_.Z() = MIN(this->min_.Z(), other.min_.Z());
	}

	void Expand(const Vector3d &point)
	{
		this->max_.X() = MAX(this->max_.X(), point.X());
		this->max_.Y() = MAX(this->max_.Y(), point.Y());
		this->max_.Z() = MAX(this->max_.Z(), point.Z());

		this->min_.X() = MIN(this->min_.X(), point.X());
		this->min_.Y() = MIN(this->min_.Y(), point.Y());
		this->min_.Z() = MIN(this->min_.Z(), point.Z());
	}

	bool Intersects(const BBox &other) const
	{
		return ((this->min_.X() >= other.min_.X() && this->min_.X() < other.max_.X() ||
			this->max_.X() >= other.min_.X() && this->max_.X() < other.max_.X()) &&
			(this->min_.Y() >= other.min_.Y() && this->min_.Y() < other.max_.Y() ||
			this->max_.Y() >= other.min_.Y() && this->max_.Y() < other.max_.Y()) &&
			(this->min_.Z() >= other.min_.Z() && this->min_.Z() < other.max_.Z() ||
			this->max_.Z() >= other.min_.Z() && this->max_.Z() < other.max_.Z()));
	}

	/* Algorithm based on article: http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter3.htm */
	bool Intersects(Ray &ray) const
	{
#ifdef DEBUG_RAY_METRICS
		ray.intersection_checks_++;
#endif
		double tNear = FLT_MIN;
		double tFar = FLT_MAX;

		if(fabs(ray.direction_.X()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.X() < this->min_.X() || ray.origin_.X() > this->max_.X())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.X() - ray.origin_.X()) / ray.direction_.X();
			double T2 = (this->max_.X() - ray.origin_.X()) / ray.direction_.X();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}

		if(fabs(ray.direction_.Y()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.Y() < this->min_.Y() || ray.origin_.Y() > this->max_.Y())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.Y() - ray.origin_.Y()) / ray.direction_.Y();
			double T2 = (this->max_.Y() - ray.origin_.Y()) / ray.direction_.Y();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}


		if(fabs(ray.direction_.Z()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.Z() < this->min_.Z() || ray.origin_.Z() > this->max_.Z())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.Z() - ray.origin_.Z()) / ray.direction_.Z();
			double T2 = (this->max_.Z() - ray.origin_.Z()) / ray.direction_.Z();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}

		return true;
	}

	bool Intersects(Ray &ray, BBoxFace &face, double &distance) const
	{
#ifdef DEBUG_RAY_METRICS
		ray.intersection_checks_++;
#endif
		double tNear = FLT_MIN;
		double tFar = FLT_MAX;

		if(fabs(ray.direction_.X()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.X() < this->min_.X() || ray.origin_.X() > this->max_.X())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.X() - ray.origin_.X()) / ray.direction_.X();
			double T2 = (this->max_.X() - ray.origin_.X()) / ray.direction_.X();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				face = FACE_PositiveX;
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}

		if(fabs(ray.direction_.Y()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.Y() < this->min_.Y() || ray.origin_.Y() > this->max_.Y())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.Y() - ray.origin_.Y()) / ray.direction_.Y();
			double T2 = (this->max_.Y() - ray.origin_.Y()) / ray.direction_.Y();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				face = FACE_PositiveY;
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}


		if(fabs(ray.direction_.Z()) < INTERSECT_EPSILON)
		{
			if(ray.origin_.Z() < this->min_.Z() || ray.origin_.Z() > this->max_.Z())
			{
				return false;
			}
		}
		else
		{
			double T1 = (this->min_.Z() - ray.origin_.Z()) / ray.direction_.Z();
			double T2 = (this->max_.Z() - ray.origin_.Z()) / ray.direction_.Z();
			if(T1 > T2)
			{
				double temp = T1;
				T1 = T2;
				T2 = temp;
			}

			if(T1 > tNear)
			{
				face = FACE_PositiveZ;
				tNear = T1;
			}

			if(T2 < tFar)
			{
				tFar = T2;
			}
			

			if(tNear > tFar || tFar < 0)
			{
				return false;
			}
		}

		if(face == FACE_PositiveX && ray.direction_.X() > 0)
			face = FACE_NegativeX;
		
		if(face == FACE_PositiveY && ray.direction_.Y() > 0)
			face = FACE_NegativeY;

		if(face == FACE_PositiveZ && ray.direction_.Z() > 0)
			face = FACE_NegativeZ;

		distance = tNear;
		return true;
	}

	/*bool Intersects2(Ray &ray, float &distance) const
    {
		
      float num1 = 0.0f;
      float num2 = FLT_MAX;
      if ((double) abs(ray.direction_.X()) < 9.99999997475243E-07)
      {
        if ((double) ray.origin_.X() < (double) min_.X() || (double) ray.origin_.X() > (double) max_.X())
          return false;
      }
      else
      {
        float num3 = 1.0f / ray.direction_.X();
        float num4 = (min_.X() - ray.origin_.X()) * num3;
        float num5 = (max_.X() - ray.origin_.X()) * num3;
        if ((double) num4 > (double) num5)
        {
          float num6 = num4;
          num4 = num5;
          num5 = num6;
        }
        num1 = MAX(num4, num1);
        num2 = MIN(num5, num2);
        if ((double) num1 > (double) num2)
          return false;
      }
      if ((double) abs(ray.direction_.Y()) < 9.99999997475243E-07)
      {
        if ((double) ray.origin_.Y() < (double) min_.Y() || (double) ray.origin_.Y() > (double) max_.Y())
          return false;
      }
      else
      {
        float num3 = 1.0f / ray.direction_.Y();
        float num4 = (min_.Y() - ray.origin_.Y()) * num3;
        float num5 = (max_.Y() - ray.origin_.Y()) * num3;
        if ((double) num4 > (double) num5)
        {
          float num6 = num4;
          num4 = num5;
          num5 = num6;
        }
        num1 = MAX(num4, num1);
        num2 = MIN(num5, num2);
        if ((double) num1 > (double) num2)
          return false;
      }
      if ((double) abs(ray.origin_.Z()) < 9.99999997475243E-07)
      {
        if ((double) ray.origin_.Z() < (double) min_.Z() || (double) ray.origin_.Z() > (double) max_.Z())
          return false;
      }
      else
      {
        float num3 = 1.0f / ray.direction_.Z();
        float num4 = (min_.Z() - ray.origin_.Z()) * num3;
        float num5 = (max_.Z() - ray.origin_.Z()) * num3;
        if ((double) num4 > (double) num5)
        {
          float num6 = num4;
          num4 = num5;
          num5 = num6;
        }
        num1 = MAX(num4, num1);
        float num7 = MIN(num5, num2);
        if ((double) num1 > (double) num7)
          return false;
      }
	  
	  distance = num1;
	  return true;
    }*/
};


#endif
