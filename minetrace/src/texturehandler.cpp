#include "texturehandler.h"
#include "texture.h"
#include "freeimage/FreeImage.h"
#include <boost/filesystem.hpp>
#include <string>

struct texture_file_desc {
	int x;
	int y;
	std::string name;
};

texture_file_desc tex_descs[] = {
	{0, 0, "lava1"},
	{1, 0, "lava2"},
	{2, 0, "water1"},
	{3, 0, "water2"},
	{4, 0, "bed1_front"},
	{5, 0, "book_case"},
	{6, 0, "cactus_bottom"},
	{7, 0, "cake_side_eaten"},
	{8, 0, "crack1"},
	{9, 0, "unknown1"},
	{10, 0, "door1_top"},
	{11, 0, "door2_top"},
	{12, 0, "rose1"},
	{13, 0, "dropper_front"},
	{14, 0, "end_portal_perl"},
	{15, 0, "purple_flower"},
	{16, 0, "acacia_planks"},
	{17, 0, "pumpkin_side"},
	{18, 0, "powered_rail_off"},
	{19, 0, "redstone_lamp_on"},
	{20, 0, "oak_sapling"},
	{21, 0, "stone_slab_side"},
	{22, 0, "lily_pad"},
	{23, 0, "blue_wool"},

	{0, 1, "lava3"},
	{1, 1, "lava4"},
	{2, 1, "water3"},
	{3, 1, "water4"},
	{4, 1, "bed1_side"},
	{5, 1, "brewing_stand"},
	{6, 1, "cactus_side"},
	{7, 1, "cake_side"},
	{8, 1, "crack2"},
	{9, 1, "dispenser_front"},
	{10, 1, "unknown2"},
	{11, 1, "fern1"},
	{12, 1, "sunflower_back"},
	{13, 1, "dropper_top"},
	{14, 1, "end_portal_side"},
	{15, 1, "blue_flower"},
	{16, 1, "dark_oak_planks"},
	{17, 1, "pumpkin_stem1"},
	{18, 1, "powered_rail_on"},
	{19, 1, "redstone_ore"},
	{20, 1, "dark_oak_sapling"},
	{21, 1, "stone_slab_top"},
	{22, 1, "cobweb"},
	{23, 1, "green_wool"},

	{0, 2, "stone"},
	{1, 2, "stone_crack1"},
	{2, 2, "stone_crack2"},
	{3, 2, "stone_crack3"},
	{4, 2, "bed1_top"},
	{5, 2, "unknown4"},
	{6, 2, "cactus_top"},
	{7, 2, "cake_top"},
	{8, 2, "crack3"},
	{9, 2, "dispenser_top"},
	{10, 2, "door3_top"},
	{11, 2, "fern2"},
	{12, 2, "sunflower_stem1"},
	{13, 2, "emerald_block"},
	{14, 2, "end_portal_top"},
	{15, 2, "yellow_flower"},
	{16, 2, "birch_planks"},
	{17, 2, "pumpkin_stem2"},
	{18, 2, "rail"},
	{19, 2, "redstone_torch_off"},
	{20, 2, "spruce_sapling"},
	{21, 2, "stone_brick1"},
	{22, 2, "wheat1"},
	{23, 2, "magenta_wool"},

	{0, 3, "unknown5"},
	{1, 3, "bed2_front"},
	{2, 3, "bed2_side"},
	{3, 3, "bed2_top"},
	{4, 3, "bedrock"},
	{5, 3, "brick"},
	{6, 3, "cake_bottom"},
	{7, 3, "unknown6"},
	{8, 3, "crack4"},
	{9, 3, "door4_bottom"},
	{10, 3, "door5_bottom"},
	{11, 3, "vines"},
	{12, 3, "sunflower_front"},
	{13, 3, "emerald_ore"},
	{14, 3, "oak_wood"},
	{15, 3, "white_flower1"},
	{16, 3, "oak_planks"},
	{17, 3, "pumpkin_top"},
	{18, 3, "rail_turn"},
	{19, 3, "redstone_torch_on"},
	{20, 3, "sea_lantern"},
	{21, 3, "stone_brick2"},
	{22, 3, "wheat2"},
	{23, 3, "orange_wool"},

	{0, 4, "carrot1"},
	{1, 4, "carrot2"},
	{2, 4, "carrot3"},
	{3, 4, "cauldron_bottom"},
	{4, 4, "cauldron_floor"},
	{5, 4, "cauldron_side"},
	{6, 4, "cauldron_top"},
	{7, 4, "unknown7"},
	{8, 4, "diamond_block"},
	{9, 4, "door4_top"},
	{10, 4, "door5_top"},
	{11, 4, "grass"},
	{12, 4, "sunflower_stem2"},
	{13, 4, "obsidian"},
	{14, 4, "spruce_wood"},
	{15, 4, "white_flower2"},
	{16, 4, "unknown7"},
	{17, 4, "unknown8"},
	{18, 4, "red_sand"},
	{19, 4, "sugar_cane"},
	{20, 4, "unknown9"},
	{21, 4, "stone_brick3"},
	{22, 4, "wheat3"},
	{23, 4, "pink_wool"},

	{0, 5, "coal_block"},
	{1, 5, "coal_ore"},
	{2, 5, "farmland_side"}, //podzol?
	{3, 5, "cobblestone"},
	{4, 5, "mossy_cobblestone"},
	{5, 5, "cocoa_beans1"},
	{6, 5, "cocoa_beans2"},
	{7, 5, "cocoa_beans3"},
	{8, 5, "diamond_ore"},
	{9, 5, "door6_bottom"},
	{10, 5, "door7_bottom"},
	{11, 5, "white_flower3_ bottom"},
	{12, 5, "pink_flower_bottom"},
	{13, 5, "enchantment_table_side"},
	{14, 5, "fern3"},
	{15, 5, "unknown9"},
	{16, 5, "spruce_planks"},
	{17, 5, "unknown10"},
	{18, 5, "red_sandstone1"},
	{19, 5, "redstone_repeater_top_off"},
	{20, 5, "unknown11"},
	{21, 5, "stone_bricks4"},
	{22, 5, "wheat4"},
	{23, 5, "purple_wool"},

	{0, 6, "command_block"},
	{1, 6, "unknown12"},
	{2, 6, "unknown13"},
	{3, 6, "crafting_table_side1"},
	{4, 6, "crafting_table_side2"},
	{5, 6, "crafting_table_top"},
	{6, 6, "daylight_sensor_inverted"},
	{7, 6, "daylight_sensor_side"},
	{8, 6, "dirt"},
	{9, 6, "door6_top"},
	{10, 6, "door7_top"},
	{11, 6, "white_flower3_ top"},
	{12, 6, "pink_flower_top"},
	{13, 6, "enchantment_table_top"},
	{14, 6, "fire1"},
	{15, 6, "rose"},
	{16, 6, "nether_portal"},
	{17, 6, "unknown14"},
	{18, 6, "red_sandstone2"},
	{19, 6, "redstone_repeater_top_on"},
	{20, 6, "soul_sand"},
	{21, 6, "unknown15"},
	{22, 6, "wheat5"},
	{23, 6, "red_wool"},

	{0, 7, "daylight_sensor"},
	{1, 7, "dead_tree"},
	{2, 7, "big_crack1"},
	{3, 7, "big_crack2"},
	{4, 7, "big_crack3"},
	{5, 7, "big_crack4"},
	{6, 7, "big_crack5"},
	{7, 7, "big_crack6"},
	{8, 7, "podzol_side"},
	{9, 7, "door1_bottom"},
	{10, 7, "door2_bottom"},
	{11, 7, "rose_bush"},
	{12, 7, "dragon_egg"},
	{13, 7, "end_stone"},
	{14, 7, "fire2"},
	{15, 7, "orange_tulip"},
	{16, 7, "potatoes1"},
	{17, 7, "unknown16"},
	{18, 7, "red_sandstone3"},
	{19, 7, "sand"},
	{20, 7, "sponge"},
	{21, 7, "tnt_bottom"},
	{22, 7, "wheat6"},
	{23, 7, "gray_wool"},

	{0, 8, "pink_tulip"},
	{1, 8, "red_tulip"},
	{2, 8, "white_tulip"},
	{3, 8, "furnace_front_off"},
	{4, 8, "furnace_front_on"},
	{5, 8, "furnace_side"},
	{6, 8, "furnace_top"},
	{7, 8, "glass"},
	{8, 8, "black_glass"},
	{9, 8, "blue_glass"},
	{10, 8, "brown_glass"},
	{11, 8, "turquoise_glass"},
	{12, 8, "gray_glass"},
	{13, 8, "green_glass"},
	{14, 8, "skyblue_glass"},
	{15, 8, "lightgreen_glass"},
	{16, 8, "potatoes2"},
	{17, 8, "unknown17"},
	{18, 8, "red_sandstone4"},
	{19, 8, "sandstone1"},
	{20, 8, "wet_sponge"},
	{21, 8, "tnt_side"},
	{22, 8, "wheat7"},
	{23, 8, "white_wool"},

	{0, 9, "magenta_glass"},
	{1, 9, "orange_glass"},
	{2, 9, "glass_side"},
	{3, 9, "black_glass_side"},
	{4, 9, "blue_glass_side"},
	{5, 9, "brown_glass_side"},
	{6, 9, "turquoise_glass_side"},
	{7, 9, "gray_glass_side"},
	{8, 9, "green_glass_side"},
	{9, 9, "skyblue_glass_side"},
	{10, 9, "lightgreen_glass_side"},
	{11, 9, "magenta_glass_side"},
	{12, 9, "orange_glass_side"},
	{13, 9, "pink_glass_side"},
	{14, 9, "purple_glass_side"},
	{15, 9, "red_glass_side"},
	{16, 9, "potatoes3"},
	{17, 9, "unknown18"},
	{18, 9, "red_sandstone5"},
	{19, 9, "sandstone2"},
	{20, 9, "stone"},
	{21, 9, "tnt_top"},
	{22, 9, "wheat8"},
	{23, 9, "yellow_wool"},

	{0, 10, "lightgray_glass_side"},
	{1, 10, "white_glass_side"},
	{2, 10, "yellow_glass_side"},
	{3, 10, "pink_glass"},
	{4, 10, "purple_glass"},
	{5, 10, "red_glass"},
	{6, 10, "lightgray_glass"},
	{7, 10, "white_glass"},
	{8, 10, "yellow_glass"},
	{9, 10, "glowstone"},
	{10, 10, "gold_block"},
	{11, 10, "gold_ore"},
	{12, 10, "grass_side"},
	{13, 10, "unknown19"}, // Tintable grass side?
	{14, 10, "snow_side"},
	{15, 10, "grass_top_tint"},
	{16, 10, "potatoes4"},
	{17, 10, "unknown20"},
	{18, 10, "redstone_block"},
	{19, 10, "sandstone3"},
	{20, 10, "unknown21"},
	{21, 10, "torch"},
	{22, 10, "black_wool"},


	{25, 18, "missing"},
};

#define NUM_TEXTURES 264

TextureHandler::TextureHandler(const std::string &minecraft_path)
	: minecraft_path_(minecraft_path)
{
}

void TextureHandler::Initialize()
{
	using namespace boost::filesystem;
	path texture_path(minecraft_path_);
	
	texture_path /= "textures_0.png";
	Texture texture0(FreeImage_Load(FIF_PNG, texture_path.c_str()));	
	texture_path = texture_path.parent_path();

	texture_path /= "textures_1.png";
	Texture texture1(FreeImage_Load(FIF_PNG, texture_path.c_str()));
	texture_path = texture_path.parent_path();

	texture_path /= "textures_2.png";
	Texture texture2(FreeImage_Load(FIF_PNG, texture_path.c_str()));
	texture_path = texture_path.parent_path();

	texture_path /= "textures_3.png";
	Texture texture3(FreeImage_Load(FIF_PNG, texture_path.c_str()));
	texture_path = texture_path.parent_path();

	texture_path /= "textures_4.png";
	Texture texture4(FreeImage_Load(FIF_PNG, texture_path.c_str()));
	texture_path = texture_path.parent_path();


	const Texture *levels[5] = {
		&texture0,
		&texture1,
		&texture2,
		&texture3,
		&texture4
	};

	for(texture_file_desc &desc : tex_descs)
	{
		LODTexturePtr texture = LoadMipMap(levels, desc.x, desc.y);
		textures_.insert(std::make_pair(desc.name, texture));
	}
}

LODTexturePtr TextureHandler::LoadMipMap(const Texture *levels[5], int x, int y)
{
	auto il = {
		FreeImage_Copy(levels[0]->bitmap(), x*16, y*16, x*16 + 16, y*16 + 16),
		FreeImage_Copy(levels[1]->bitmap(), x*8, y*8, x*8 + 8, y*8 +8),
		FreeImage_Copy(levels[2]->bitmap(), x*4, y*4, x*4 + 4, y*4 + 4),
		FreeImage_Copy(levels[3]->bitmap(), x*2, y*2, x*2 + 2, y*2 + 2),
		FreeImage_Copy(levels[4]->bitmap(), x, y, x + 1, y + 1)
	};
	return std::make_shared<LODTexture>(il);
}

LODTexturePtr &TextureHandler::GetTexture(const std::string &name)
{
	auto it = textures_.find(name);
	if(it == textures_.end())
		return textures_["missing"];
	else
		return it->second;
}
