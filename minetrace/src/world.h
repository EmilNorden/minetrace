#ifndef WORLD_H_
#define WORLD_H_

#include <memory>
#include <vector>
#include <cstdint>
#include <string>
#include "vector.h"
#include "octree.h"

class Camera;
class Region;
class Block;

class World
{
private:
	std::string path_;
	std::shared_ptr<Camera> camera_;
	std::vector<std::shared_ptr<Region>> loaded_regions_;
	std::vector<std::shared_ptr<Octree<std::shared_ptr<Block>>>> octrees_;
//	std::vector<std::shared_ptr<Block>> light_sources_;
public:
	std::string path() const { return path_; }
	void set_path(const std::string &path) { path_ = path; }
	std::shared_ptr<Camera> camera() const { return camera_; }
	void set_camera(std::shared_ptr<Camera> &camera) { camera_ = camera; }
//	const std::vector<std::shared_ptr<Block>> &light_sources() const { return light_sources_; }
	void LoadRegions();
	bool CastRay(Ray &ray, IntersectionInfo<std::shared_ptr<Block>> &intersection);

	static Vector2i region_to_chunk_coord(const Vector2i &coord);
	static Vector2i chunk_to_block_coord(const Vector2i &coord);

	uint32_t block_count() const;
};

#endif
