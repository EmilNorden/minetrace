#ifndef SHADERS_H_
#define SHADERS_H_

#include "intersection_info.h"
#include "world.h"
#include "vector.h"
#include <memory>

class TextureHandler;

class Ray;
class RayTracerSettings;
void shade(const Ray &ray, const IntersectionInfo<std::shared_ptr<Block>> &intersection, const std::shared_ptr<World> &world, const std::shared_ptr<TextureHandler> &textures, const RayTracerSettings &settings, Vector4d &out_color);
void shade_sky(Vector4d &out_color);

#endif
