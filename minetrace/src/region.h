#ifndef REGION_H_
#define REGION_H_

#include <map>
#include <cstdint>
#include <string>
#include "nbt/nbt_tag.h"
#include "vector.h"

class Region
{
private:
	std::map<int16_t, int32_t> locations_;
	std::map<int16_t, int32_t> timestamps_;
	bool loaded_;
	Vector2i coordinate_;

	void LoadFile(const std::string &path);
public:
	Region();
	~Region();

	std::map<int16_t, NBTTagPtr> data_;

	Vector2i coordinate() const { return coordinate_; }
	bool loaded() const { return loaded_; }
	void LoadFile(const std::string &folder, int x, int z);
};

#endif