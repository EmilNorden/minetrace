#include "texture.h"
#include "freeimage/FreeImage.h"
#include "massert.h"
#include "vector.h"

#include <cstdint>
#include <cmath>

Texture::Texture(FIBITMAP *texture)
	: texture_(texture)
{
	width_ =  FreeImage_GetWidth(texture_);
	height_ = FreeImage_GetHeight(texture_);

	texel_density_.X() = 1.0f / width_;
	texel_density_.Y() = 1.0f / height_;
}

Texture::~Texture()
{
	FreeImage_Unload(texture_);
}

double Texture::Round(double x)
{
	return floor(x + 0.5);
}

double Texture::IEEERemainder(double x, double y)
{
	return x - (y * Texture::Round(x / y));
}

void Texture::Sample(Vector4d &color, double u, double v, double mipmap_factor) const
{
	// No wrapping, only accept [0,1]
	ASSERT(u >= 0 && u <= 1.0f);
	ASSERT(v >= 0 && v <= 1.0f);

	RGBQUAD rgb;
	FreeImage_GetPixelColor(texture_, u * (width_ - 1), v * (height_ - 1), &rgb);
	//return rgb;
	color.X() = rgb.rgbRed / 255.0;
	color.Y() = rgb.rgbGreen / 255.0;
	color.Z() = rgb.rgbBlue / 255.0;
	color.W() = rgb.rgbReserved / 255.0;
}

void Texture::SampleBilinear(double u, double v, Vector4d &color) const
{
	// How many texels do we have per pixel?

    // remainderX and remainderY tells us how much of the neighbouring pixels we are "touching".
    double remainderX = Texture::IEEERemainder(u, texel_density_.X());
    double remainderY = Texture::IEEERemainder(v, texel_density_.Y());

    // Subtract with the remainders, so the UV coordinates are positioned ON a texel.
    u -= remainderX;
    v -= remainderY;

    // Get the pixel xy coordinates for our texel.
    int x = (int)(u * (width_ - 1));
    int y = (int)(v * (height_ - 1));

    // Get the pixel xy coordinates for the surrounding pixels.
    int x2 = (int)((u + texel_density_.X()) * (width_ - 1));
    int y2 = (int)((v + texel_density_.Y()) * (height_ - 1));

    // Extract all four colors; our "base" color, the one below, the one to the side, and the one diagonally down.
    int baseArgb, blendXargb, blendYargb, blendXYargb;

	RGBQUAD baseColor, blendXColor, blendYColor, blendXYColor;
	BOOL asd = FreeImage_GetPixelColor(texture_, x, y, &baseColor);
	asd = FreeImage_GetPixelColor(texture_, x2, y, &blendXColor);
	asd = FreeImage_GetPixelColor(texture_, x, y2, &blendYColor);
	asd = FreeImage_GetPixelColor(texture_, x2, y2, &blendXYColor);
    // Interpolate between the four colors.
	Vector3i colorXDifference;
	colorXDifference.X() = static_cast<int32_t>(blendXColor.rgbBlue) - baseColor.rgbBlue;
	colorXDifference.Y() = static_cast<int32_t>(blendXColor.rgbGreen) - baseColor.rgbGreen;
	colorXDifference.Z() = static_cast<int32_t>(blendXColor.rgbRed) - baseColor.rgbRed;

	Vector3i colorYDifference;
	colorYDifference.X() = static_cast<int32_t>(blendYColor.rgbBlue) - baseColor.rgbBlue;
	colorYDifference.Y() = static_cast<int32_t>(blendYColor.rgbGreen) - baseColor.rgbGreen;
	colorYDifference.Z() = static_cast<int32_t>(blendYColor.rgbRed) - baseColor.rgbRed;

	Vector3i colorXYDifference;
	colorXYDifference.X() = static_cast<int32_t>(blendXYColor.rgbBlue) - baseColor.rgbBlue;
	colorXYDifference.Y() = static_cast<int32_t>(blendXYColor.rgbGreen) - baseColor.rgbGreen;
	colorXYDifference.Z() = static_cast<int32_t>(blendXYColor.rgbRed) - baseColor.rgbRed;

    double dx = (remainderX * width_) + 0.5;
    double dy = (remainderY * height_) + 0.5;
    double invertDx = 1.0f - dx;
    double invertDy = 1.0f - dy;

    // The final color is multiplied with BYTE_RECIPROCAL (1 / 255) instead of being divided by 255. It has the same effect but is apparently
    // slightly more efficient.
	color.W() = 1.0f;
	color.Z() = ((static_cast<int32_t>(baseColor.rgbBlue) * invertDx * invertDy) +
		(static_cast<int32_t>(blendYColor.rgbBlue) * invertDx * dy) +
		(static_cast<int32_t>(blendXColor.rgbBlue) * dx * invertDy) +
		(static_cast<int32_t>(blendXYColor.rgbBlue) * dx * dy)) / 255.0f;

	color.Y() = ((static_cast<int32_t>(baseColor.rgbGreen) * invertDx * invertDy) +
		(static_cast<int32_t>(blendYColor.rgbGreen) * invertDx * dy) +
		(static_cast<int32_t>(blendXColor.rgbGreen) * dx * invertDy) +
		(static_cast<int32_t>(blendXYColor.rgbGreen) * dx * dy)) / 255.0f;

	color.X() = ((static_cast<int32_t>(baseColor.rgbRed) * invertDx * invertDy) +
		(static_cast<int32_t>(blendYColor.rgbRed) * invertDx * dy) +
		(static_cast<int32_t>(blendXColor.rgbRed) * dx * invertDy) +
		(static_cast<int32_t>(blendXYColor.rgbRed) * dx * dy)) / 255.0f;
}

void Texture::SetPixel(int x, int y, const Vector4d &color, bool additive)
{
	RGBQUAD rgb;
	
	rgb.rgbRed = color.X() * 255;
	rgb.rgbGreen = color.Y() * 255;
	rgb.rgbBlue = color.Z() * 255;
	rgb.rgbReserved = color.W() * 255;

	if(additive)
	{
		RGBQUAD orig_rgb;
		FreeImage_GetPixelColor(texture_, x, y, &orig_rgb);
		rgb.rgbRed += orig_rgb.rgbRed;
		rgb.rgbGreen += orig_rgb.rgbGreen;
		rgb.rgbBlue += orig_rgb.rgbBlue;
		rgb.rgbReserved += orig_rgb.rgbReserved;
	}

	FreeImage_SetPixelColor(texture_, x, y, &rgb);
}

Texture *Texture::Clone() const
{
	FIBITMAP *cloned_image = FreeImage_Clone(texture_);
	ASSERT(cloned_image != nullptr);
	return new Texture(cloned_image);
}

LODTexture::LODTexture(FIBITMAP *texture, std::vector<TexturePtr> &levels)
	: Texture(texture)
{
	for(TexturePtr &level : levels)
	{
		levels_.push_back(std::move(level));
	}	
}

LODTexture::LODTexture(std::initializer_list<FIBITMAP*> list)
	: Texture(*list.begin())
{
	ASSERT(list.size() > 0);
	
	auto it = std::begin(list);
	++it; // Skip first

	for(auto end = std::end(list); it != end; ++it)
	{
		levels_.push_back(std::make_shared<Texture>(*it));
	}

	for(FIBITMAP *bitmap : list)
	{
	//	TexturePtr texture = std::make_shared<Texture>(bitmap);
	//	levels_.push_back(std::make_shared<Texture>(bitmap));
	}
}
