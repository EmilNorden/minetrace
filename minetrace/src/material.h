#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <memory>

class Texture;

class Material
{
private:
	Texture *diffuse_;
	Texture *emissive_;
public:
	const Texture *diffuse() const;
	const Texture *emissive() const;

};

#endif