#ifndef SCENE_H_
#define SCENE_H_

#include <memory>

class Camera;

class Scene
{
private:
	std::shared_ptr<Camera> camera_;
public:
	std::shared_ptr<Camera> get_camera() const;
	void set_camera(std::shared_ptr<Camera> &camera);

};

#endif
