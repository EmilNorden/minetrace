#ifndef BLOCK_TYPES_H_
#define BLOCK_TYPES_H_

#include <cstdint>
#include <string>

struct BlockTraits
{
public:
	bool is_full_size;
	bool has_texture_alpha;
};

#ifdef _MSC_VER

#pragma warning(push)
#pragma warning( disable : 4480 ) // disable warning that is generated when explicitly specifying underlying enum type. This is a C++11 feature and shouldn't generate a warning.

#endif

enum BlockType : uint8_t
{
	BT_Air,
	BT_Stone,
	BT_GrassBlock,
	BT_Dirt,
	BT_Cobblestone,
	BT_WoodPlanks,
	BT_Saplings,
	BT_Bedrock,
	BT_Water,
	BT_StationaryWater,
	BT_Lava,
	BT_StationaryLava,
	BT_Sand,
	BT_Gravel,
	BT_GoldOre,
	BT_IronOre,
	BT_CoalOre,
	BT_Wood,
	BT_Leaves,
	BT_Sponge,
	BT_Glass,
	BT_LapisLazuliOre,
	BT_LapisLazuliBlock,
	BT_Dispenser,
	BT_Sandstone,
	BT_NoteBlock,
	BT_Bed,
	BT_PoweredRail,
	BT_DetectorRail,
	BT_StickyPiston,
	BT_Cobweb,
	BT_Grass,
	BT_DeadBush,
	BT_Piston,
	BT_PistonExtension,
	BT_Wool,
	BT_BlockMovedByPiston,
	BT_Dandelion,
	BT_Rose,
	BT_BrownMushroom,
	BT_RedMushroom,
	BT_BlockOfGold,
	BT_BlockOfIron,
	BT_DoubleSlabs,
	BT_Slabs,
	BT_Bricks,
	BT_TNT,
	BT_Bookshelf,
	BT_MossStone,
	BT_Obsidian,
	BT_Torch,
	BT_Fire,
	BT_MonsterSpawner,
	BT_OakWoodStairs,
	BT_Chest,
	BT_RedstoneWire,
	BT_DiamondOre,
	BT_BlockOfDiamond,
	BT_Craftingtable,
	BT_Wheat,
	BT_Farmland,
	BT_Furnace,
	BT_BurningFurnace,
	BT_SignPost,
	BT_WoodenDoor,
	BT_Ladders,
	BT_Rail,
	BT_CobblestoneStairs,
	BT_WallSign,
	BT_Lever,
	BT_StonePressurePlate,
	BT_IronDoor,
	BT_WoodenPressurePlate,
	BT_RedstoneOre,
	BT_GlowingRedstoneOre,
	BT_RedstoneTorchInactive,
	BT_RedstoneTorchActive,
	BT_StoneButton,
	BT_Snow,
	BT_Ice,
	BT_SnowBlock,
	BT_Cactus,
	BT_Clay,
	BT_SugarCane,
	BT_Jukebox,
	BT_Fence,
	BT_Pumpkin,
	BT_Netherrack,
	BT_SoulSand,
	BT_Glowstone,
	BT_NetherPortal,
	BT_JackOLantern,
	BT_CakeBlock,
	BT_RedstoneRepeaterInactive,
	BT_RedstoneRepeaterActive,
	BT_LockedChest,
	BT_Trapdoor,
	BT_MonsterEgg,
	BT_StoneBricks,
	BT_HugeBrownMushroom,
	BT_HugeRedMushroom,
	BT_IronBars,
	BT_GlassPane,
	BT_Melon,
	BT_PumpkinStem,
	BT_MelonStem,
	BT_Vines,
	BT_FenceGate,
	BT_BrickStairs,
	BT_StoneBrickStairs,
	BT_Mycelium,
	BT_LilyPad,
	BT_NetherBrick,
	BT_NetherBrickFence,
	BT_NetherBrickStairs,
	BT_NetherWart,
	BT_EnchantmentTable,
	BT_BrewingStand,
	BT_Cauldron,
	BT_EndPortal,
	BT_EndPortalBlock,
	BT_EndStone,
	BT_DragonEgg,
	BT_RedstoneLampInactive,
	BT_RedstoneLampActive,
	BT_WoodenDoubleSlab,
	BT_WoodenSlab,
	BT_Cocoa,
	BT_SandstoneStairs,
	BT_EmeraldOre,
	BT_EnderChest,
	BT_TripwireHook,
	BT_Tripwire,
	BT_BlockOfEmerald,
	BT_SpruceWoodStairs,
	BT_BirchWoodStairs,
	BT_JungleWoodStairs,
	BT_CommandBlock,
	BT_Beacon,
	BT_CobblestoneWall,
	BT_FlowerPot,
	BT_Carrots,
	BT_Potatoes,
	BT_WoodenButton,
	BT_MobHead,
	BT_Anvil,
	BT_TrappedChest,
	BT_WeightedPressurePlateLight,
	BT_WeightedPressurePlateHeavy,
	BT_RedstoneComparatorInactive,
	BT_RedstoneComparatorActive,
	BT_DaylightSensor,
	BT_BlockOfRedstone,
	BT_NetherQuartzOre,
	BT_Hopper,
	BT_BlockOfQuarts,
	BT_QuartStairs,
	BT_ActivatorRail,
	BT_Dropper,
	BT_StainedClay,
	BT_StainedGlassPane,
	BT_LeavesAcaciaDarkOak,
	BT_WoodAcaciaDarkOak,
	BT_NumberOfBlockTypes
};

#define BD_Wood_Oak				0
#define BD_Wood_Spruce			1
#define BD_Wood_Birch			2
#define BD_Wood_Jungle			3

#define BD_Wood_Dir_Normal			0
#define BD_Wood_Dir_EastWest		1
#define BD_Wood_Dir_NorthSouth		2
#define BD_Wood_Dir_Directionless	3

#define BD_Block_Dir_Down		0
#define BD_Block_Dir_Up			1
#define BD_Block_Dir_North		2
#define BD_Block_Dir_South		3
#define BD_Block_Dir_West		4
#define BD_Block_Dir_East		5

#define BD_Sandstone_Normal		0
#define BD_Sandstone_Chiseled	1
#define BD_Sandstone_Smooth		2

#define BD_Bed_Head_Flag		8

#define BD_Wool_White			0
#define BD_Wool_Orange			1
#define BD_Wool_Magenta			2
#define BD_Wool_LightBlue		3
#define BD_Wool_Yellow			4
#define BD_Wool_Lime			5
#define BD_Wool_Pink			6
#define BD_Wool_Gray			7
#define BD_Wool_LightGray		8
#define BD_Wool_Cyan			9
#define BD_Wool_Purple			10
#define BD_Wool_Blue			11
#define BD_Wool_Brown			12
#define BD_Wool_Green			13
#define BD_Wool_Red				14
#define BD_Wool_Black			15

#define BD_Slab_Stone			0
#define BD_Slab_Sandstone		1
#define BD_Slab_Wooden			2
#define BD_Slab_Cobblestone		3
#define BD_Slab_Brick			4
#define BD_Slab_StoneBrick		5
#define BD_Slab_NetherBrick		6
#define BD_Slab_Quartz			7
#define BD_Slab_SmoothStone		8
#define BD_Slab_SmoothSandstone	9
#define BD_Slab_TileQuartz		21

#define BD_Pumpkin_Face_South	0
#define BD_Pumpkin_Face_West	1
#define BD_Pumpkin_Face_North	2
#define BD_Pumpkin_Face_East	3
#define BD_Pumpkin_Face_None	4

#define BD_StoneBrick_Normal	0
#define BD_StoneBrick_Mossy		1
#define BD_StoneBrick_Cracked	2
#define BD_StoneBrick_Chiseled	3

#define BD_WoodAcacia_Normal	0
#define BD_WoodDarkOak_Normal	1
#define BD_WoodAcacia_EastWest	4
#define BD_WoodDarkOak_EastWest	5
#define BD_WoodAcacia_SouthNorth 8
#define BD_WoodDarkOak_SouthNorth 9
#define BD_WoodAcacia_Directionless 12
#define BD_WoodDarkOak_Directionless 13

const BlockTraits block_traits[] =
{
	{false, false},
	{true, false}, // stone
	{true, false}, // grass block
	{true, false}, // dirt
	{true, false}, // cobblestone
	{true, false}, // woood planks
	{false, true}, // saplings
	{true, false}, // bedrock
	{false, true}, // Water
	{true, true}, // Stationary water (Always full block?)
	{true, false}, // lava
	{true, false}, // stationary lava
	{true, false}, // sand
	{true, false}, // gravel
	{true, false}, // gold ore
	{true, false}, // iron ore
	{true, false}, // coal ore
	{true, false}, // wood
	{true, true}, // Leaves
	{true, false}, // sponge
	{true, true}, // glass
	{true, false}, // lapis ore
	{true, false}, // lapis block
	{true, false}, // dispenser
	{true, false}, // sandstone
	{true, false}, // noteblock
	{false, true}, // bed
	{false, true}, // powered rail
	{false, true}, // activator rail
	{true, false}, // sticky piston
	{true, true}, // cobweb
	{false, true}, // Grass
	{false, true}, // Dead bush
	{true, false}, // Piston
	{false, false}, // Piston extension
	{true, false}, // wool
	{true, false}, // block moved by piston
	{false, true}, // dandelion
	{false, true}, // rose
	{false, true}, // brown mushroom
	{false, true}, // red mushroom
	{true, false}, // block of gold
	{true, false}, // block of iron
	{true, false}, // double slabs
	{false, false}, // slabs
	{true, false}, // bricks
	{true, false}, // TNT
	{true, false}, // bookshelf
	{true, false}, // moss stone
	{true, false}, // obsidian
	{false, false}, // torch
	{true, true}, // Fire
	{true, true}, // Monster spawner
	{false, false}, // oakwoodstairs
	{false, false}, // chest
	{false, false}, // redstone wire
	{true, false}, // diamond ore
	{true, false}, // crafting table
	{false, true}, // wheat
	{false, false}, // Farmland
	{true, false}, // furnace
	{true, false}, // burning furnace
	{false, false}, // sign post
	{false, true}, // wooden door
	{false, true}, // ladder
	{false, true}, // rail
	{false, false}, // cobblestone stairs
	{false, false}, // wallsign
	{false, false}, // lever
	{false, false}, // stone pressure plate
	{false, true}, // iron door
	{false, false}, // wooden pressure plate
	{true, false}, // redstone ore
	{true, false}, // glowing redstone ore
	{false, false}, // redstone torch
	{false, false}, // redstone torch active
	{false, false}, // stone button
	{false, false}, // snow
	{true, true}, // ice
	{true, false}, // snow block
	{true, false}, // cactus
	{true, false}, // clay
	{true, false}, // sugar cane (FULL BLOCK?)
	{true, false}, // juke box
	{false, false}, // fence
	{true, false}, // pumpkin
	{true, false}, // netherrack
	{true, false}, // soul sand
	{true, false}, // glowstone
	{false, true}, // nether portal
	{true, false}, // jack o lantern
	{false, false}, // cake
	{false, false}, // repeater inactive
	{false, false}, // repeater active
	{false, false}, // locked chest (CHEST - FULL BLOCK?)
	{false, true}, // trap door
	{true, false}, // monster egg
	{true, false}, // stone bricks
	{false, false}, // Huge brown mushroom (FULL BLOCK?)
	{false, false}, // Huge red mushroom (FULL BLOCK?)
	{false, true}, // iron bars
	{false, true}, // glass pane
	{true, false}, // melon
	{false, true}, // pumpkin stem
	{false, true}, // melon stem
	{false, true}, // vines
	{false, false}, // fence gate
	{false, false}, // brick stairs
	{false, false}, // stone brick stairs
	{true, false}, // mycelium
	{false, true}, // lily pad
	{true, false}, // nether brick
	{false, false}, // nether brick fence
	{false, false}, // nether brick stairs
	{false, true}, // nether wart
	{false, false}, // enchantment table
	{false, false}, // Brewing stand (alpha?)
	{false, true}, // Cauldron
	{false, true}, // End portal
	{false, false}, // end portal block
	{true, false}, // end stone
	{false, false}, // dragon egg
	{true, false}, // redstone lamp inactive
	{true, false}, // redstone lamp active
	{true, false}, // wooden double slab
	{false, false}, // wooden slab
	{false, false}, // cocoa
	{false, false}, // sandstone stairs
	{true, false}, // emerald ore
	{false, false}, // ender chest (FULL BLOCK?)
	{false, false}, // tripwire hook
	{false, true}, // tripwire
	{true, false}, // Block of emerald
	{false, false}, // spruce wood stairs
	{false, false}, // birch wood stairs
	{false, false}, // jungle wood stairs
	{true, false}, // command block
	{true, true}, // Beacon
	{false, false}, // cobblestone wall
	{false, false}, // flower pot
	{false, true}, // Carrots
	{false, true}, // Potatoes
	{false, false}, // wooden button
	{false, false}, // mob head
	{false, false}, // anvil
	{false, false}, // trapped chest (Full block?)
	{false, false}, // weighted pressure plate light
	{false, false} // weighted pressure plate heavy
};


#ifdef _MSC_VER

#pragma warning(pop)

#endif

inline BlockType operator++( BlockType &rs, int ) {
   return rs = static_cast<BlockType>(rs + 1);
}

const std::string block_textures[][16] = {
	{},
	{"stone"},
	{"grass_side", "grass_top", "dirt"},
	{"dirt"},
	{"stonebrick"},
	{"wood", "wood_birch", "wood_jungle", "wood_spruce"},
	{"sapling", "sapling_birch", "sapling_jungle", "sapling_spruce"},
	{"bedrock"},
	{"water", "water_flow"},
	{"water", "water_flow"},
	{"lava", "lava_flow"},
	{"lava", "lava_flow"},
	{"sand"},
	{"gravel"},
	{"oreGold"},
	{"oreIron"},
	{"oreCoal"},
	{"tree_side", "tree_jungle", "tree_spruce", "tree_top"},
	{"leaves", "leaves_jungle", "leaves_spruce"},
	{"sponge"},
	{"glass"},
	{"oreLapis"},
	{"blockLapis"},
	{"dispenser_front", "dispenser_front_vertical", "furnace_side", "furnace_top"},
	{"sandstone_side", "sandstone_top", "sandstone_bottom"},
	{"musicBlock"},
	{"bed_feet_end", "bed_feet_side", "bed_feet_top", "bed_head_end", "bed_head_side", "bed_head_top"},
	{"activatorRail", "activatorRail_powered"},
	{"piston_bottom", "piston_inner_top", "piston_side", "piston_top_sticky"},
	{"web"},
	{"tallgrass"},
	{"deadbush"},
	{"piston_bottom", "piston_inner_top", "piston_side", "piston_top"},
	{}, // Piston Extension
	{"cloth_0", "cloth_1", "cloth_2", "cloth_3", "cloth_4", "cloth_5", "cloth_6", "cloth_7", "cloth_8", "cloth_9", "cloth_10", "cloth_11", "cloth_12", "cloth_13", "cloth_14", "cloth_15"},
	{}, // Block moved by piston
	{"flower"},
	{"rose"},
	{"mushroom_brown"},
	{"mushroom_red"},
	{"blockGold"},
	{"blockIron"},
	{"stoneslab_side", "stoneslab_top", "sandstone_smooth"}, // Alot of other slab types, but they are already in the array, cant be bothered typing them all in again.
	{"brick"},
	{"tnt_bottom","tnt_side", "tnt_top"},
	{"bookshelf"},
	{"stoneMoss"},
	{"obsidian"},
	{"torch"},
	{"fire_0", "fire_1"},
	{"mobSpawner"},
	{} // oak wood stairs

};


#endif
