#include "output_helper.h"
#include <iostream>
#include <iomanip>

void print_progress(float percentage, int width, bool print_percentage)
{
	std::cout << static_cast<char>(13) << '[';
	int progress = percentage * width;
	int remaining = width - progress;
	for(int i = 0; i < progress; ++i)
		std::cout << '*';

	for(int i = 0; i < remaining; ++i)
		std::cout << ' ';

	std::cout << ']';

	if(print_percentage)
	{
		std::cout << ' ' << static_cast<int>(percentage * 100) << '%';
	}
}