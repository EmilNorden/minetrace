#ifndef OCTREE_CUBOID_H_
#define OCTREE_CUBOID_H_

#include "bbox.h"
#include <boost/shared_array.hpp>
#include <forward_list>
#include "vector.h"

template <typename T>
class Cuboid
{
public:
	int item_counter_;
	int id_;
	std::forward_list<T> items_;
	boost::shared_array<Cuboid> children_;
	BBox bounds_;

	void set_id(int id) { id_ = id; }
	int id() const { return id_; }

	Cuboid()
		: item_counter_(0)
	{
	}

	inline
	void add(T item)
	{
		items_.push_front(item);
		item_counter_++;
	}

	inline
	void remove()
	{
		item_counter_--;
	}
};

#endif