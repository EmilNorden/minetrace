#ifndef UTIL_H_
#define UTIL_H_

#include <chrono>
#include <string>
#include <iostream>

class stopwatch
{
private:
	std::chrono::high_resolution_clock::time_point start_;
	std::string name_;
public:
	stopwatch(const std::string &name)
		: name_(name)
	{
		start_ = std::chrono::high_resolution_clock::now();
	}

	~stopwatch()
	{
		std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start_);
		std::cout << "Stopwatch [" << name_ << "] - " << time_span.count() << " seconds." << std::endl;
	}
};

#endif